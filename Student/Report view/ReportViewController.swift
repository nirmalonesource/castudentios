//
//  ReportViewController.swift
//  Student
//
//  Created by My Mac on 20/02/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class ReportViewController: UIViewController, UITableViewDelegate , UITableViewDataSource {
    @IBOutlet weak var imgBG: UIImageView!
    @IBAction func back_click(_ sender: UIButton) {
         self.navigationController?.popViewController(animated: true)
    }
    @IBOutlet var vwtop: UIView!
    @IBOutlet var btnperformance: UIButton!
    
    @IBOutlet var vwmiddle: UIView!
    @IBOutlet var lblcompleted: UILabel!
    @IBOutlet var lblmissed: UILabel!
    @IBOutlet var lbltotal: UILabel!
    
    @IBOutlet var tblreportlist: UITableView!
    @IBOutlet var tblheight: NSLayoutConstraint!
    
    var finishedLoadingInitialTableCells = false
    var ReportListArr:NSMutableArray = []
    var ReportData:NSMutableDictionary = [:]
    
    override func viewDidLayoutSubviews() {
         tblheight.constant = tblreportlist.contentSize.height
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imgBG.setGredient()
        btnperformance.setRadius(radius: btnperformance.frame.size.width/2)
        btnperformance.layer.borderWidth = 3.0
        btnperformance.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
        
        tblreportlist.rowHeight = UITableView.automaticDimension
        tblreportlist.estimatedRowHeight = 600
        vwtop.roundCorners(corners: [.topLeft,.topRight], radius: 10)
       // tblreportlist.roundCorners(corners: [.bottomLeft,.bottomRight], radius: 10)
        TEST_SUMMARY_DETAILS_Api()
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ReportListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:ReportCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ReportCell
        cell.layoutIfNeeded()
//        cell.vwBG.setShadow()
        
        cell.layoutIfNeeded()
        cell.selectionStyle = .none
        let dic:NSDictionary = ReportListArr.object(at: indexPath.row) as! NSDictionary
        cell.lblsubjectname.text = dic.value(forKey: "SubjectName") as? String
        cell.lblaccuracy.text = String(format: "Accuracy %@%%", dic.value(forKey: "OverallPerformance") as? String ?? "0") //
        cell.lblcompleted.text = dic.value(forKey: "TotalAttempt") as? String
        cell.lblmissed.text = dic.value(forKey: "TotalMissed") as? String
        
        let OverallPerformance = Float((dic.value(forKey: "OverallPerformance") as? NSString)?.floatValue ?? 0)
                    let floatid = Float(OverallPerformance/100)
                    cell.progressvw.setProgress(floatid, animated: true)
        
       
        
//        if  test_details1["subjectaccuracy"] != nil {
//
//            let idTo = Float((test_details1["subjectaccuracy"]! as! NSString).floatValue)
//            let floatid = Float(idTo/100)
//            cell.ProgressView.setProgress(floatid, animated: true)
//            print("PROCESS:",cell.ProgressView.progress);
//            var String1 = "Your Overall Subject Accuracy: "
//            String1 += test_details1["subjectaccuracy"]! as! String
//            String1 += "%"
//
//            cell.AccuracyLabel.text = String1
//        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dic:NSDictionary = ReportListArr.object(at: indexPath.row) as! NSDictionary

        if ((dic.value(forKey: "OverallPerformance") as? String) != nil)
        {
            let dic:NSDictionary = ReportListArr.object(at: indexPath.row) as! NSDictionary
            let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "DetailReportView") as! DetailReportView
            secondVc.SubjectID = dic.value(forKey: "SubjectID") as? String ?? "0"
            secondVc.SubjectName = dic.value(forKey: "SubjectName") as? String ?? "0"
            self.navigationController?.pushViewController(secondVc, animated: true)
        }
        else
        {
            showToast(uiview: self, msg: "Assignment Not Available")
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var lastInitialDisplayableCell = false
        if 3 > 0 && !finishedLoadingInitialTableCells {
            if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        
        if !finishedLoadingInitialTableCells {
            if lastInitialDisplayableCell {
                finishedLoadingInitialTableCells = true
            }
            cell.loadAnimation()
        }
    }
    func setdata()  {
        
      //  btnperformance.setTitle(ReportData.value(forKey: "OverallPerformance") as? String, for: .normal)
        btnperformance.setTitle(String(format: "%@%%", ReportData.value(forKey: "OverallPerformance") as? String ?? ""), for: .normal)
        lbltotal.text = String(format: "Total:-%@", ReportData.value(forKey: "TotalAssign") as? String ?? "")
        lblmissed.text = ReportData.value(forKey: "TotalMissed") as? String
        lblcompleted.text = ReportData.value(forKey: "TotalAttempt") as? String
    }
    
    func TEST_SUMMARY_DETAILS_Api()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
           // let parameters = ["PaperID" : PaperID] as [String : Any]
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["student"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.GET_TESTSUMMARYDETAILS)!,
                              method: .post,
                              parameters: nil,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                return
                        }
                        
                        resData = data
                        
                        if resData["status"] as? Bool ?? false
                        {
                            self.ReportData = (resData["data"] as! NSDictionary ).mutableCopy() as! NSMutableDictionary
                            self.ReportListArr = (self.ReportData.value(forKey: "Subject") as! NSArray).mutableCopy() as! NSMutableArray
                            self.setdata()
                            self.tblreportlist.reloadData()
                        }
                        
                        //  showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }

}

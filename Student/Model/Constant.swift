//
//  Constant.swift
//  JetPayApp
//
//  Created by My Mac on 2/14/18.
//  Copyright © 2018 My Mac. All rights reserved.
//

import Foundation
import UIKit

struct MyClassConstants
{
    static var dictGlobalData: [String : AnyObject] = [:]
}

struct CustomerConstants
{
    static var user_id = String()

}

//MARK:- Screen Size

struct ScreenSize
{
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0

}

//MARK:- Custom fonts
struct FontNames {
    
//    static let RajdhaniName = "rajdhani"
//    struct Rajdhani {
        static let RajdhaniBold = "rajdhani-bold"
        static let RajdhaniMedium = "rajdhani-medium"
//    }
}

 //MARK:- Webservices

   /*
 student/paperquestionlist
   Confirm OTP student/resendotp , student/confirmotp
   = "http://laxraj.com/digitalupleta/"
   */
struct Appstore {
    static let APP_ID = "1452058297"
    static let APPSTOREREVIEW_LINK = "itms-apps://itunes.apple.com/app/viewContentsUserReviews?id=\(APP_ID)"
}

 struct ServiceList
 {
//    static let SERVICE_URL = "https://test.childrens.parshvaa.com/api/v2/"
//    static let WEBVIEW_URL = "https://test.childrens.parshvaa.com/"
    
    static let SERVICE_URL = "https://cagssmart.com/api/v1/"
    static let WEBVIEW_URL = "https://cagssmart.com/"

    static let X_CI_CHILDRENS_ACADEMY_API_KEY = "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss"
    static let LOGIN = "student/login"
    static let USERNAME = "CHILDRENS-ACADEMY-API"
    static let PASSWORD = "API@CHILDRENS123"
    static let RESEND_OTP = "student/login"
    static let CONFIRM_OTP = "student/confirmotp"
    static let QUERYMESSAGE = "querymessage"
    static let LEGAL = "legal/student"
    static let GET_PAPER_LIST = "student/paperlist"
    static let GET_PAPER_QUESTION_LIST = "student/paperquestionlist"
    static let GET_VIDEO_PAPER_QUESTION_LIST = "student/paperquestionlist"
    static let GET_NOTIFICATION_LIST = "notification_list"
    //static let IMAGE_URL = "folder_path"
    static let ZOOKI_LIST = "zooki"
    static let PROFILE = "profile"
    static let STUDENT_TEST_REPORT = "student/test_report"
    static let STUDENT_TEST_SUMMARY = "student/test_summary"

    static let LEVEL_LIST = "level"
    static let SUBJECT_LIST = "student/subject-list"
    static let STUDENT_SUBMIT_TEST = "student/submit_test"
    static let STUDENT_SEARCH_PAPER = "student/searchpaper"
    static let GET_PROFILE = "student/profile"
    
    
    static let GET_Assesment = "student/AssessmentPaperlist"
    static let get_libraryAssesment  = "student/LibraryAssessment"
    
    static let GET_TESTSUMMARYDETAILS = "student/Test_summary_details"
    static let GET_SUBJECTSTUDENTPLANNER = "student/Subject_student_planner"
    
}




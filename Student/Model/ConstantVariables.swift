//
//  ConstantVariables.swift
//  JetPayApp
//
//  Created by My Mac on 2/14/18.
//  Copyright © 2018 My Mac. All rights reserved.
//

import UIKit
import Foundation

class ConstantVariables: NSObject {
    struct Constants {
        static let Project_Name = "Student App"
        static let currency_INR = "₹"
        
        static let navigationColor =  0xF16E4B
        static let lightGreen = 0xF1546E
        
        static let radius : CGFloat = 15

        static let appDelegate = UIApplication.shared.delegate as! AppDelegate

    }
    
    struct LocalizeString {
        static let PLEASE_ENTER = "Please Enter"
        static let OTP = "Otp"
        static let OTP_NOT_MATCH = "Otp Not Match"
        static let STUDENT_CODE = "UID"
        static let MESSAGE = "message"
    }
}

extension UserDefaults{
    
    func setShowTutorial(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isTutorial.rawValue)
    }
    
    func getIsTutorial()-> Bool {
        return bool(forKey: UserDefaultsKeys.isTutorial.rawValue)
    }
    
    func setIsLogin(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isUserLogin.rawValue)
    }
    
    func getIsLogin()-> Bool {
        return bool(forKey: UserDefaultsKeys.isUserLogin.rawValue)
    }
    
    func setUserDict(value: [String : Any]){
        set(value, forKey: UserDefaultsKeys.userData.rawValue)
    }
    
    func getUserDict() -> [String : Any]{
        return dictionary(forKey: UserDefaultsKeys.userData.rawValue) ?? [:]
    }
   
}

enum UserDefaultsKeys : String {
    
    case userData
    case isUserLogin
    case isTutorial

}

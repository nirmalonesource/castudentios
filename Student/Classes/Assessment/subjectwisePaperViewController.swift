//
//  subjectwisePaperViewController.swift
//  Student
//
//  Created by Mac on 06/04/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit


class subjectwisePaperCell: UITableViewCell {
    
    
    @IBOutlet weak var vwBG: UIView!
   
    @IBOutlet weak var lblSubmissionOn: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgArr: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}



class subjectwisePaperViewController: UIViewController ,UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var lbltitle: UILabel!
    var subjectWise_chapter = [[String : Any]]()
     var finishedLoadingInitialTableCells = false
    var titlename : String?
    
    @IBOutlet weak var nodatavw:UIView!
    
    
    @IBOutlet weak var imgGredient: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      imgGredient.setGredient()
       
        lbltitle.text = titlename
        
        print(subjectWise_chapter)
       if self.subjectWise_chapter.count > 0
                                     {
        self.nodatavw.isHidden = true
                                    
       }
            else
        {
         self.nodatavw.isHidden = false
                                     }
        // Do any additional setup after loading the view.
    }
    

    // Mark TableView
       
          func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                   return subjectWise_chapter.count
               }
               
               func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
               {
                  let cell:subjectwisePaperCell = tableView.dequeueReusableCell(withIdentifier: "subjectwisePaperCell") as! subjectwisePaperCell
                            //  cell.layoutIfNeeded()
                              
                cell.lblTitle.text = subjectWise_chapter[indexPath.row]["PlannerName"] as? String
       
        
                              // cell.lblMonth.text = month
                               cell.imgArr.changeImageViewImageColor(color: UIColor.init(rgb: ConstantVariables.Constants.navigationColor))
                              cell.imgArr.transform = cell.imgArr.transform.rotated(by: CGFloat(-CGFloat.pi))

                              let df = subjectWise_chapter[indexPath.row]["PublishDate"] as? String ?? ""
                if df == ""
                {
                    
                }
                else{
                              let getDate = setDateWithFormat(strDate: df)
                              cell.lblSubmissionOn.text = "Submission on : " + getDate
                            
                }
                              
                              
                              
                              
                              if indexPath.row == 0
                              {
                                  cell.vwBG.roundCorners(corners: [.topLeft , .topRight], radius: 10)
                              }
                              else if indexPath.row == 3
                              {
                                  cell.vwBG.roundCorners(corners: [.bottomLeft , .bottomRight], radius: 10)
                              }
                              else
                              {
                                  cell.vwBG.roundCorners(corners: [.bottomLeft , .bottomRight], radius: 0)
                              }
                              
                            //  cell.layoutIfNeeded()
                              cell.selectionStyle = .none
                              return cell


                   
                   if indexPath.row == 0
                   {
                       cell.vwBG.roundCorners(corners: [.topLeft , .topRight], radius: 10)
                   }
                   else if indexPath.row == 3
                   {
                       cell.vwBG.roundCorners(corners: [.bottomLeft , .bottomRight], radius: 10)
                   }
                   else
                   {
                       cell.vwBG.roundCorners(corners: [.bottomLeft , .bottomRight], radius: 0)
                   }
                   
                 //  cell.layoutIfNeeded()
                   cell.selectionStyle = .none
                   return cell
               }
               
               func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                 
       //
       //            let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "HintView") as! HintView
       //            secondVc.saveddata = [student_Assessment_List[indexPath.row]]
       //            secondVc.Get_PaperID = student_Assessment_List[indexPath.row]["MCQPlannerID"] as? String ?? ""
       //            secondVc.strSubject = student_Assessment_List[indexPath.row]["SubjectName"] as? String ?? ""
       //            self.navigationController?.pushViewController(secondVc, animated: true)
                   
               }
               
               func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
                   var lastInitialDisplayableCell = false
                   if subjectWise_chapter.count > 0 && !finishedLoadingInitialTableCells {
                       if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows,
                           let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {
                           lastInitialDisplayableCell = true
                       }
                   }
                   
                   if !finishedLoadingInitialTableCells {
                       if lastInitialDisplayableCell {
                           finishedLoadingInitialTableCells = true
                       }
                       cell.loadAnimation()
                   }
               }
       
        @IBAction func btnBackAction(_ sender: UIButton) {
              self.navigationController?.popViewController(animated: true)
          }
    func setDateWithFormat(strDate : String) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d-MMM-yyyy"
        //"MMM dd ,yyyy HH:mm"
        let date: Date? = dateFormatterGet.date(from: strDate)!
        return dateFormatter.string(from: date!)
    }
    
    func setDayWithFormat(strDate : String) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dMMM"
        //"MMM dd ,yyyy HH:mm"
        let date: Date? = dateFormatterGet.date(from: strDate)!
        return dateFormatter.string(from: date!)
    }
    
    func setMonthWithFormat(strDate : String) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM"
        //"MMM dd ,yyyy HH:mm"
        let date: Date? = dateFormatterGet.date(from: strDate)!
        return dateFormatter.string(from: date!)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

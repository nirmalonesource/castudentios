//
//  ReportCell.swift
//  Student
//
//  Created by My Mac on 14/09/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit

class ReportCell: UITableViewCell {
    @IBOutlet var lblsubjectname: UILabel!
    @IBOutlet var lblaccuracy: UILabel!
    @IBOutlet var progressvw: UIProgressView!
    @IBOutlet var lblcompleted: UILabel!
    @IBOutlet var lblmissed: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

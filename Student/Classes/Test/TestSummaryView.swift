//
//  TestSummaryView.swift
//  Student
//
//  Created by archana on 06/02/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class TestSummaryLevelcolletionCell: UICollectionViewCell {
    
 
}

class TestSummaryView: UIViewController
{

    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var webview: UIWebView!
    @IBOutlet var btnvideo: UIButton!
    
   
    var finishedLoadingInitialCollectionCells = false
    var summary_detail = [String : Any]()
    var dataLevel = [[String : Any]]()
    
    var strID = ""

    //MARK:- WEB SERVICE
    
    func get_Test_Summary()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let parameters = ["PaperID" : strID] as [String : Any]
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["student"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.STUDENT_TEST_SUMMARY)!,
                              method: .post,
                              parameters: parameters,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                return
                        }
                        
                        resData = data
                        
                        if resData["status"] as? Bool ?? false
                        {
                            self.summary_detail = resData["data"] as? [String : Any]  ?? [:]
                            self.dataLevel = [self.summary_detail["test_details"] as? [String : Any] ?? [:]]
                            
                            if self.dataLevel[0]["Type"] as! String == "1"
                            {
                                self.btnvideo.isHidden = false
                            }
                            else
                            {
                                self.btnvideo.isHidden = true
                            }
                        }
                        
                        
                       // showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imgBG.setGredient()
        get_Test_Summary()
        
        let weburl = ServiceList.WEBVIEW_URL
        let userid = (UserDefaults.standard.getUserDict()["student"] as AnyObject).value(forKey: "id") as? String ?? ""
        //let url = WEBVIEW_URL+
        
        let first = weburl + "test_summary?UserID=" + userid + "&PaperID=" + strID
        
        let myURL = URL(string: first)
        let myRequest = URLRequest(url: myURL!)
        webview.loadRequest(myRequest)
       // test_summary?UserID=20&PaperID=20
        
        // Do any additional setup after loading the view.
        
       
        let image = UIImage(named: "videoicon")?.withRenderingMode(.alwaysTemplate)
        btnvideo.setImage(image, for: .normal)
        btnvideo.tintColor = UIColor.white
        
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
   
    @IBAction func video_click(_ sender: UIButton) {
        
        let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "TestQuestionListView") as! TestQuestionListView
        secondVc.saveddata = dataLevel
        secondVc.isfromsummary = true
        self.navigationController?.pushViewController(secondVc, animated: true)
    }

}

//
//  MyProfileView.swift
//  TeacherApp
//
//  Created by archana on 31/01/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class MyProfileView: UIViewController {

    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var vwPopupDetail: UIView!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    
    @IBOutlet weak var lblProgress: UILabel!
    @IBOutlet weak var progressBR: UIProgressView!
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtBoard: UITextField!
    @IBOutlet weak var txtMeduim: UITextField!
    @IBOutlet weak var txtStandard: UITextField!
    @IBOutlet weak var txtBranch: UITextField!
    @IBOutlet weak var txtBatch: UITextField!
    var dataProfile = [String : Any]()

    //MARK:- Web Service
    
    func getProfile()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["student"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.GET_PROFILE)!,
                              method: .get,
                              parameters: nil,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
	                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        if resData["status"] as? Bool ?? false
                        {
                            self.dataProfile = resData["data"] as? [String:Any] ?? [:]
                            self.txtName.text = "\(self.dataProfile["firstname"] as? String ?? "")" + "\(self.dataProfile["lastname"] as? String ?? "")"
                            self.txtEmail.text = "\(self.dataProfile["email"] as? String ?? "")"
                            self.txtMobile.text = "\(self.dataProfile["mobileno"] as? String ?? "")"
                            self.txtBoard.text = "\(self.dataProfile["BoardName"] as? String ?? "")"
                            self.txtMeduim.text = "\(self.dataProfile["MediumName"] as? String ?? "")"
                            self.txtStandard.text = "\(self.dataProfile["StandardName"] as? String ?? "")"
                            self.txtBranch.text = "\(self.dataProfile["BranchName"] as? String ?? "")"
                            self.txtBatch.text = "\(self.dataProfile["BatchName"] as? String ?? "")"
                            
                            
                            self.lblUserName.text = self.txtName.text
                            self.lblMobileNo.text = "\(self.dataProfile["username"] as? String ?? "")"
                        }
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.layoutIfNeeded()
        imgBG.setGredient()
        
        getProfile()
        
        vwPopupTop.roundCorners(corners: [.topLeft , .topRight], radius: 10)
        vwPopupDetail.roundCorners(corners: [.bottomLeft , .bottomRight], radius: 10)

        imgUser.setRadius(radius: imgUser.frame.height/2)
        imgUser.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.lightGreen))
        self.view.layoutIfNeeded()
        
    }
    
    @IBAction func btnMenuAction(_ sender: UIButton) {
        let popOverVC = self.storyboard?.instantiateViewController(withIdentifier: "MenuPageViewController") as! MenuPageViewController
        
        self.addChild(popOverVC)
        let sd  = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        
        popOverVC.view.frame = sd
        //self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
        // self.view.bringSubviewToFront(popOverVC.view)
        UIApplication.shared.keyWindow!.bringSubviewToFront(popOverVC.view)
    }
}

//
//  Created by Pete Smith
//  http://www.petethedeveloper.com
//
//
//  License
//  Copyright © 2017-present Pete Smith
//  Released under an MIT license: http://opensource.org/licenses/MIT
//

import UIKit
import ScalingCarousel
import Alamofire
import SVProgressHUD
import SDWebImage

struct Zooki {
    var Title: String?
    var Descript: String?
    var Image: String?
    var CreatedDate: String?
    init(Title: String, Descript: String, Image: String, CreatedDate: String) {
        self.Title = Title
        self.Descript = Descript
        self.Image = Image
        self.CreatedDate = CreatedDate
    }
}

class Cell: ScalingCarouselCell {
    
    @IBOutlet weak var imgVw: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
}

class StoryboardViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var carousel: ScalingCarouselView!
    @IBOutlet weak var lblCurrentSelection: UILabel!
    @IBOutlet weak var imgBG: UIImageView!
    var folder_path = String()
    var dataToDisplay: [Zooki] = []
    var zookiList = [[String : Any]]()
    
    //    @IBOutlet weak var carouselBottomConstraint: NSLayoutConstraint!
    //    @IBOutlet weak var output: UILabel!
    
    private struct Constants {
        static let carouselHideConstant: CGFloat = -250
        static let carouselShowConstant: CGFloat = 15
    }
    
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ZookiApi()
        imgBG.setGredient()
    }
    
    //MARK:- WEB SERVICES
    
    func ZookiApi()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            SVProgressHUD.show(withStatus: nil)
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["student"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
           
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.ZOOKI_LIST)!,
                              method: .get,
                              parameters: nil,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : Any] = [:]
                        guard let data = response.result.value as? [String:Any],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        if resData["status"] as? Bool ?? false
                        {
                            self.zookiList = resData["data"] as! [[String : Any]]
                            self.folder_path = resData["folder_path"] as? String ?? ""
                              for i in 0..<self.zookiList.count {
                                  let Title = self.zookiList[i]["Title"] as? String ?? ""
                                  let Descript = self.zookiList[i]["Description"] as? String ?? ""
                                  let Image = self.zookiList[i]["Image"] as? String ?? ""
                                  let CreatedDate = self.zookiList[i]["CreatedDate"] as? String ?? ""
                                
                                 self.dataToDisplay.append(Zooki(Title: Title, Descript: Descript, Image: Image, CreatedDate: CreatedDate))
                            }
                        }
                        
                        self.lblCurrentSelection.text = String(describing: 1) + "/\(self.zookiList.count)"

                       // showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        
                        DispatchQueue.main.async {
                            self.carousel.reloadData()
                        }
                        
                        self.view.layoutIfNeeded()
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        carousel.deviceRotated()
    }
    
    // MARK: - Button Actions
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func showHideButtonPressed(_ sender: Any) {
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        })
    }
}

typealias CarouselDatasource = StoryboardViewController
extension CarouselDatasource: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataToDisplay.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! Cell

        cell.lblTitle.text = dataToDisplay[indexPath.row].Title
        cell.lblDate.text = dataToDisplay[indexPath.row].CreatedDate
        cell.lblDescription.text = dataToDisplay[indexPath.row].Descript
       // let imgURL = dataToDisplay[indexPath.row].Image
        
        if let scalingCell = cell as? ScalingCarouselCell {
            scalingCell.mainView.backgroundColor = .white
            scalingCell.mainView.clipsToBounds = true
        }
        
        let folderath = folder_path + "\(zookiList[indexPath.row]["Image"] as? String ?? "")"
        print("folder_path :\(folderath)")
        cell.imgVw.sd_setImage(with: URL(string : (folderath.URLQueryAllowedString()!)), placeholderImage : nil , options: .progressiveDownload,
                               completed: nil)
        
        DispatchQueue.main.async {
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
        }
        
            return cell
    }
}

typealias CarouselDelegate = StoryboardViewController
extension StoryboardViewController: UICollectionViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //carousel.didScroll()
        
        guard let currentCenterIndex = carousel.currentCenterCellIndex?.row else { return }
        
        lblCurrentSelection.text = String(describing: currentCenterIndex + 1) + "/\(dataToDisplay.count))"
    }
}

private typealias ScalingCarouselFlowDelegate = StoryboardViewController
extension ScalingCarouselFlowDelegate: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
}


//
//  HintView.swift
//  Student
//
//  Created by My Mac on 28/03/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit

class HintView: UIViewController,UITableViewDelegate,UITableViewDataSource,CollapsibleTableViewHeaderDelegate
{
    @IBOutlet weak var imgbg: UIImageView!
    @IBOutlet weak var lbltitle: UILabel!
    @IBAction func back_click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)

    }
    @IBAction func starttest_click(_ sender: UIButton) {
       let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "TestQuestionListView") as! TestQuestionListView
       secondVc.Get_PaperID = Get_PaperID
       secondVc.strSubject = strSubject
        secondVc.saveddata = saveddata
       self.navigationController?.pushViewController(secondVc, animated: true)
    }
    
    @IBOutlet weak var tblheight: NSLayoutConstraint!
    @IBOutlet weak var viewbg: UIView!
    @IBOutlet weak var lbltotlque: UILabel!
    
    var sections = sectionsData
    var saveddata = [[String : Any]]()
    var Questionarr:NSArray = []
    var Get_PaperID = ""
    var strSubject = String()
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var tbllist: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
          imgbg.setGredient()
        viewbg.roundCorners(corners: [.topLeft , .topRight], radius: 10)
        scrollview.roundCorners(corners: [.topLeft , .topRight], radius: 10)

        print("SAVEDDATA:",saveddata)
        lbltotlque.text = "Total Questions :- " + String(saveddata[0]["TotalQuestion"] as! String)
        lbltitle.text =  String(saveddata[0]["SubjectName"] as! String)
        Questionarr = saveddata[0]["chapter"] as! NSArray
         print("Questionarr:",Questionarr)
        for i in 0..<self.Questionarr.count {

            sections.append((Section(name: "iPad", items: [
                Item(name: "iPad Pro", detail: "iPad Pro delivers epic power, in 12.9-inch and a new 10.5-inch size."),
                ])))
//            sectionsData.append(Section(name: "iPad", items: [
//                    Item(name: "iPad Pro", detail: "iPad Pro delivers epic power, in 12.9-inch and a new 10.5-inch size."),
//                    ]))
        }
      
    }
    override func viewWillLayoutSubviews() {
      
       
    }
    
    override func viewDidLayoutSubviews() {
        tbllist.layoutIfNeeded()

        self.tblheight.constant = self.tbllist.contentSize.height
        print("TBLHEIGHT:",self.tbllist.contentSize.height)
     //   tbllist.heightAnchor.constraint(equalToConstant: tbllist.contentSize.height).isActive = true
        self.scrollview.resizeScrollViewContentSize()
     //   scrollview.contentSize = CGSize(width: self.scrollview.frame.size.width, height: self.tbllist.contentSize.height + self.tbllist.frame.origin.y)
        super.updateViewConstraints()
        tbllist.layoutIfNeeded()

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Questionarr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // return sections[section].collapsed ? 0 : sections[section].items.count
      //  sections[section] = ((((Questionarr.object(at: section)) as AnyObject).value(forKey: "sub_chapter") as! NSArray) as! [Item])
       //  return sections[section].collapsed ? 0 : sections[section].items.count
//        print("SUB CHAPTER:",((Questionarr.object(at: section)) as AnyObject).value(forKey: "sub_chapter") as! NSArray)
        return sections[section].collapsed ? 0 : (((Questionarr.object(at: section)) as AnyObject).value(forKey: "sub_chapter") as! NSArray).count
      //  return (((Questionarr.object(at: section)) as AnyObject).value(forKey: "sub_chapter") as! NSArray).count
    }
    
    // Cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CollapsibleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as? CollapsibleTableViewCell ??
            CollapsibleTableViewCell(style: .default, reuseIdentifier: "cell")
        
       // let item: Item = sections[indexPath.section].items[indexPath.row]
      //  cell.nameLabel.text = item.name
        
        let mydict:NSDictionary = Questionarr.object(at: indexPath.section) as! NSDictionary
        let arr:NSArray = mydict.value(forKey: "sub_chapter") as! NSArray
        cell.nameLabel.text = "      * " + ((arr.object(at: indexPath.row) as AnyObject) .value(forKey: "ChapterName") as! String)
    //   viewDidLayoutSubviews()
        //  cell.detailLabel.text = item.detail
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        viewDidLayoutSubviews()

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    // Header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as? CollapsibleTableViewHeader ?? CollapsibleTableViewHeader(reuseIdentifier: "header")
      
      //  header.titleLabel.text = sections[section].name
        header.arrowLabel.text = ">"
        header.setCollapsed(sections[section].collapsed)
        header.section = section
        header.titleLabel.text = "* " + String(((Questionarr.object(at:section) as AnyObject).value(forKey: "ChapterName")) as? String ?? "")

        header.delegate = self
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
      func toggleSection(_ header: CollapsibleTableViewHeader, section: Int)
      {
        let collapsed = !sections[section].collapsed
        
        // Toggle collapse
        sections[section].collapsed = collapsed
        header.setCollapsed(collapsed)
        tbllist.setNeedsLayout()
        tbllist.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
        tbllist.setNeedsLayout()
        viewDidLayoutSubviews()

    }
    
}

extension UIScrollView {
    
    func resizeScrollViewContentSize() {
        
        var contentRect = CGRect.zero
        
        for view in self.subviews {
            
            contentRect = contentRect.union(view.frame)
            
        }
        
        self.contentSize = contentRect.size
    }
    
}
class MyOwnTableView: UITableView {
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        return self.contentSize
    }
    
    override var contentSize: CGSize {
        didSet{
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override func reloadData() {
        super.reloadData()
        self.invalidateIntrinsicContentSize()
    }

}


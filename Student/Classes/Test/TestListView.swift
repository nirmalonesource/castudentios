//
//  TestListView.swift
//  Student
//
//  Created by My Mac on 30/01/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class TestListCell: UITableViewCell {
    
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var vwBG: UIView!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var vwRounder: UIView!
    @IBOutlet weak var lblExpiresOn: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgArr: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

class TestListView: UIViewController , UITableViewDelegate , UITableViewDataSource {

    @IBOutlet var nodatavw: UIView!
    @IBOutlet weak var imgGredient: UIImageView!
    @IBOutlet weak var tblVW: UITableView!
    var finishedLoadingInitialTableCells = false
    var student_Paper_List = [[String : Any]]()
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Paper_List_Api()
    
        imgGredient.setGredient()
    }
    
    //MARK:- WEB SERVICES
    
    func Paper_List_Api()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            //let parameters = ["PaperID" : "41"] as [String : Any]
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["student"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.GET_PAPER_LIST)!,
                              method: .post,
                              parameters: nil,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        if resData["status"] as? Bool ?? false
                        {
                            self.student_Paper_List = resData["data"] as! [[String : Any]]
                            print("student_Paper_List:",self.student_Paper_List)
                            if self.student_Paper_List.count > 0
                            {
                                self.nodatavw.isHidden = true
                            }
                            else
                            {
                                self.nodatavw.isHidden = false
                            }
                        }
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                          self.tblVW.reloadData()
//                        DispatchQueue.main.async {
//
//                        }
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    
    func setDateWithFormat(strDate : String) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d-MMM-yyyy"
        //"MMM dd ,yyyy HH:mm"
        let date: Date? = dateFormatterGet.date(from: strDate)!
        return dateFormatter.string(from: date!)
    }
    
    func setDayWithFormat(strDate : String) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dMMM"
        //"MMM dd ,yyyy HH:mm"
        let date: Date? = dateFormatterGet.date(from: strDate)!
        return dateFormatter.string(from: date!)
    }
    
    func setMonthWithFormat(strDate : String) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM"
        //"MMM dd ,yyyy HH:mm"
        let date: Date? = dateFormatterGet.date(from: strDate)!
        return dateFormatter.string(from: date!)
    }
    
    //MARK:- Tableview methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return student_Paper_List.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:TestListCell = tableView.dequeueReusableCell(withIdentifier: "TestListCell") as! TestListCell
      //  cell.layoutIfNeeded()
        
        cell.lblTitle.text = student_Paper_List[indexPath.row]["SubjectName"] as? String
        let Day = student_Paper_List[indexPath.row]["StartDate"] as? String ?? ""
        let Month = student_Paper_List[indexPath.row]["StartDate"] as? String ?? ""
       
         let day = setDayWithFormat(strDate: Day)
        // let month = setMonthWithFormat(strDate: Month)
         cell.lblDay.text = day
        // cell.lblMonth.text = month
         cell.imgArr.changeImageViewImageColor(color: UIColor.init(rgb: ConstantVariables.Constants.navigationColor))
        cell.imgArr.transform = cell.imgArr.transform.rotated(by: CGFloat(-CGFloat.pi))

        let df = student_Paper_List[indexPath.row]["PublishDate"] as? String ?? ""
        let getDate = setDateWithFormat(strDate: df)
        cell.lblExpiresOn.text = "Submission on : " + getDate
      
        cell.vwRounder.setRadius(radius: cell.vwRounder.frame.height/2)
        
        if indexPath.row == 0
        {
            cell.vwBG.roundCorners(corners: [.topLeft , .topRight], radius: 10)
        }
        else if indexPath.row == 3
        {
            cell.vwBG.roundCorners(corners: [.bottomLeft , .bottomRight], radius: 10)
        }
        else
        {
            cell.vwBG.roundCorners(corners: [.bottomLeft , .bottomRight], radius: 0)
        }
        
      //  cell.layoutIfNeeded()
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {        
//        let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "TestQuestionListView") as! TestQuestionListView
//        secondVc.Get_PaperID = student_Paper_List[indexPath.row]["MCQPlannerID"] as? String ?? ""
//        secondVc.strSubject = student_Paper_List[indexPath.row]["SubjectName"] as? String ?? ""
//        self.navigationController?.pushViewController(secondVc, animated: true)
        
        let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "HintView") as! HintView
        secondVc.saveddata = [student_Paper_List[indexPath.row]]
        secondVc.Get_PaperID = student_Paper_List[indexPath.row]["MCQPlannerID"] as? String ?? ""
        secondVc.strSubject = student_Paper_List[indexPath.row]["SubjectName"] as? String ?? ""
        self.navigationController?.pushViewController(secondVc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var lastInitialDisplayableCell = false
        if student_Paper_List.count > 0 && !finishedLoadingInitialTableCells {
            if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        
        if !finishedLoadingInitialTableCells {
            if lastInitialDisplayableCell {
                finishedLoadingInitialTableCells = true
            }
            cell.loadAnimation()
        }
    }
    
    //MARK:- Button Action
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//
//  NoticationViewController.swift
//  Student
//
//  Created by My Mac on 05/07/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class notificationcell: UITableViewCell {
    @IBOutlet var lbltitle: UILabel!
    @IBOutlet var lblsubtitle: UILabel!
    @IBOutlet var lbldate: UILabel!
    
    
    
}

class NoticationViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var imgbg: UIImageView!
    @IBOutlet var tblnotification: UITableView!
    var notificationlist:NSArray = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         imgbg.setGredient()
        Notification_List_Api()
    }
    

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationlist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! notificationcell
        let dic = notificationlist.object(at: indexPath.row) as! NSDictionary
        
        cell.lbltitle.text = dic.value(forKey: "NotificationTitle") as? String
        cell.lblsubtitle.text = dic.value(forKey: "NotificationMsg") as? String
        let df = dic.value(forKey: "CreatedDate") as! String
        let getDate = setDateWithFormat(strDate: df)
        cell.lbldate.text = getDate
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func setDateWithFormat(strDate : String) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d-MMM-yyyy"
        //"MMM dd ,yyyy HH:mm"
        let date: Date? = dateFormatterGet.date(from: strDate)!
        return dateFormatter.string(from: date!)
    }
    func Notification_List_Api()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            //let parameters = ["PaperID" : "41"] as [String : Any]
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["student"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.GET_NOTIFICATION_LIST)!,
                              method: .get,
                              parameters: nil,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        if resData["status"] as? Bool ?? false
                        {
                            self.notificationlist = resData["data"]  as! NSArray
                           
                        }
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                        self.tblnotification.reloadData()
                        //                        DispatchQueue.main.async {
                        //
                        //                        }
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }

    @IBAction func back_click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

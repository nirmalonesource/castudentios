//
//  LibraryViewController.swift
//  Student
//
//  Created by Mac on 06/04/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD


class LIBRARYcell: UITableViewCell {
    
    @IBOutlet weak var Subject: UILabel!
    @IBOutlet weak var vwBG: UIView!
    @IBOutlet weak var lblvideos: UILabel!
    @IBOutlet weak var imgArr: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}


class LibraryViewController: UIViewController ,UITableViewDataSource , UITableViewDelegate {

    
         @IBOutlet weak var btnlibrary: UIButton!
         @IBOutlet var nodatavw: UIView!
         @IBOutlet weak var imgGredient: UIImageView!
         @IBOutlet weak var tblVW: UITableView!
         var finishedLoadingInitialTableCells = false
         var student_LibraryAssessment_List = [[String : Any]]()
         
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        imgGredient.setGredient()
        getStudentLibraryAssessment()
        // Do any additional setup after loading the view.
    }
   
    func getStudentLibraryAssessment()
        {
            if !isInternetAvailable(){
                noInternetConnectionAlert(uiview: self)
            }
                
            else {
                
                SVProgressHUD.show(withStatus: nil)
                
                //let parameters = ["PaperID" : "41"] as [String : Any]
                let username = ServiceList.USERNAME
                let password = ServiceList.PASSWORD
                let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
                let base64LoginData = loginData.base64EncodedString()
                let headers = ["Authorization": "Basic \(base64LoginData)",
                    "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                    "User-Id" : (UserDefaults.standard.getUserDict()["student"] as AnyObject).value(forKey: "id") as? String ?? "",
                    "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
                
                Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.get_libraryAssesment)!,
                                  method: .post,
                                  parameters: nil,
                                  headers: headers).responseJSON
                    { (response:DataResponse) in
                        switch(response.result) {
                        case .success(let data):
                            print(" i got my Data Yup..",data)
                            
                            var resData : [String : AnyObject] = [:]
                            guard let data = response.result.value as? [String:AnyObject],
                                let _ = data["status"]! as? Bool
                                else{
                                    print("Malformed data received from fetchAllRooms service")
                                    SVProgressHUD.dismiss()
                                    
                                    return
                            }
                            
                            resData = data
                            if resData["status"] as? Bool ?? false
                            {
                                self.student_LibraryAssessment_List = resData["data"] as! [[String : Any]]
                                print("student_LibraryAssessment_List:",self.student_LibraryAssessment_List)
                                if self.student_LibraryAssessment_List.count > 0
                                {
                                    self.nodatavw.isHidden = true
                                }
                                else
                                {
                                    self.nodatavw.isHidden = false
                                }
                            }
                            else
                            {
                                showToast(uiview: self, msg: resData["message"] as? String ?? "")
                            }
                              self.tblVW.reloadData()
    //                        DispatchQueue.main.async {
    //
    //                        }
                            
                            SVProgressHUD.dismiss()
                        case .failure(let error):
                            print(error)
                            SVProgressHUD.dismiss()
                        }
                }
            }
        }
    
      //MARK:- Tableview methods
            
            func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                return student_LibraryAssessment_List.count
            }
            
            func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
            {
                let cell:LIBRARYcell = tableView.dequeueReusableCell(withIdentifier: "LIBRARYcell") as! LIBRARYcell
              //  cell.layoutIfNeeded()
                
                cell.Subject.text = student_LibraryAssessment_List[indexPath.row]["SubjectName"] as? String
                cell.lblvideos.text = ("\(student_LibraryAssessment_List[indexPath.row]["SubjectTotalAssessment"] as? Int ?? 0)") + " Videos"
               
                 
                // cell.lblMonth.text = month
                 cell.imgArr.changeImageViewImageColor(color: UIColor.init(rgb: ConstantVariables.Constants.navigationColor))
                cell.imgArr.transform = cell.imgArr.transform.rotated(by: CGFloat(-CGFloat.pi))

                
              
                
                  
                         if indexPath.row == 0
                         {
                             cell.vwBG.roundCorners(corners: [.topLeft , .topRight], radius: 10)
                         }
                         else if indexPath.row == 3
                         {
                             cell.vwBG.roundCorners(corners: [.bottomLeft , .bottomRight], radius: 10)
                         }
                         else
                         {
                             cell.vwBG.roundCorners(corners: [.bottomLeft , .bottomRight], radius: 0)
                         }
                
              //  cell.layoutIfNeeded()
                cell.selectionStyle = .none
                return cell
            }
            
            func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "SubjectViewController") as! SubjectViewController
                secondVc.subject_Chapter_List = student_LibraryAssessment_List[indexPath.row]["Chapter_Info"] as? [[String:Any]] ?? []
                secondVc.Subjectname = student_LibraryAssessment_List[indexPath.row]["SubjectName"] as? String
                  self.navigationController?.pushViewController(secondVc, animated: true)
                
            }
    
            
            func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
                var lastInitialDisplayableCell = false
                if student_LibraryAssessment_List.count > 0 && !finishedLoadingInitialTableCells {
                    if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows,
                        let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {
                        lastInitialDisplayableCell = true
                    }
                }
                
                if !finishedLoadingInitialTableCells {
                    if lastInitialDisplayableCell {
                        finishedLoadingInitialTableCells = true
                    }
                    cell.loadAnimation()
                }
            }
            
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func btn_back(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
        
    }
    
}

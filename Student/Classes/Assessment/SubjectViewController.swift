//
//  SubjectViewViewController.swift
//  Student
//
//  Created by Mac on 06/04/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit



class SubjectlistCell: UITableViewCell {


@IBOutlet weak var vwBG: UIView!
@IBOutlet weak var lblTitle: UILabel!
@IBOutlet weak var imgArr: UIImageView!

override func awakeFromNib() {
    super.awakeFromNib()
}
    
}
class SubjectViewController: UIViewController,UITableViewDataSource , UITableViewDelegate {

    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet var nodatavw: UIView!
      @IBOutlet weak var imgGredient: UIImageView!
      @IBOutlet weak var tblVW: UITableView!
      var finishedLoadingInitialTableCells = false
    var Subjectname : String?
     
    var subject_Chapter_List = [[String : Any]]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      print(subject_Chapter_List)
        imgGredient.setGredient()
   
        
        lbltitle.text = Subjectname
        
        if self.subject_Chapter_List.count > 0
                                      {
                                          self.nodatavw.isHidden = true
                                      }
                                      else
                                      {
                                          self.nodatavw.isHidden = false
                                      }
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        tblVW.reloadData()
    }

  
    // Mark TableView
    
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                return subject_Chapter_List.count
            }
            
            func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
            {
                let cell:SubjectlistCell = tableView.dequeueReusableCell(withIdentifier: "SubjectlistCell") as! SubjectlistCell
              //  cell.layoutIfNeeded()
                
                cell.lblTitle.text = subject_Chapter_List[indexPath.row]["ChapterName"] as? String
           
            cell.imgArr.changeImageViewImageColor(color: UIColor.init(rgb: ConstantVariables.Constants.navigationColor))
                cell.imgArr.transform = cell.imgArr.transform.rotated(by: CGFloat(-CGFloat.pi))


                
                if indexPath.row == 0
                {
                    cell.vwBG.roundCorners(corners: [.topLeft , .topRight], radius: 10)
                }
                else if indexPath.row == 3
                {
                    cell.vwBG.roundCorners(corners: [.bottomLeft , .bottomRight], radius: 10)
                }
                else
                {
                    cell.vwBG.roundCorners(corners: [.bottomLeft , .bottomRight], radius: 0)
                }
                
              //  cell.layoutIfNeeded()
                cell.selectionStyle = .none
                return cell
            }
            
            func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
              
    
                let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "subjectwisePaperViewController") as! subjectwisePaperViewController
                print(subject_Chapter_List)
                secondVc.titlename  = subject_Chapter_List[indexPath.row]["ChapterName"] as? String
              secondVc.subjectWise_chapter = subject_Chapter_List[indexPath.row]["Paper_info"] as? [[String:Any]] ?? []
                self.navigationController?.pushViewController(secondVc, animated: true)
                
                
    //            secondVc.saveddata = [student_Assessment_List[indexPath.row]]
    //            secondVc.Get_PaperID = student_Assessment_List[indexPath.row]["MCQPlannerID"] as? String ?? ""
    //            secondVc.strSubject = student_Assessment_List[indexPath.row]["SubjectName"] as? String ?? ""
    //            self.navigationController?.pushViewController(secondVc, animated: true)
                
            }
            
            func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
                var lastInitialDisplayableCell = false
                if subject_Chapter_List.count > 0 && !finishedLoadingInitialTableCells {
                    if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows,
                        let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {
                        lastInitialDisplayableCell = true
                    }
                }
                
                if !finishedLoadingInitialTableCells {
                    if lastInitialDisplayableCell {
                        finishedLoadingInitialTableCells = true
                    }
                    cell.loadAnimation()
                }
            }
    
     @IBAction func btnBackAction(_ sender: UIButton) {
           self.navigationController?.popViewController(animated: true)
       }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

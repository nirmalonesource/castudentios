//
//  TestQuestionListView.swift
//  Student
//
//  Created by archana on 04/02/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SDWebImage
import AVKit
import JMMarkSlider

protocol SubCategoryProductDelegate : class {
    func didPressButton(arr : [String:Any] , index : Int)
}

struct QuestionList {
    var Question: String?
    init(Question: String) {
        self.Question = Question
    }
}

struct OptionsList {
    var Options: String?
    var IsCorrect: String?
    var MCQOptionID: String?
    var MCQQuestionID: String?

    init(Options: String) {
        self.Options = Options
    }
    init(IsCorrect: String) {
        self.IsCorrect = IsCorrect
    }
    init(MCQOptionID: String) {
        self.MCQOptionID = MCQOptionID
    }
    init(MCQQuestionID: String) {
        self.MCQQuestionID = MCQQuestionID
    }
}

class TestQuestionListCell: UITableViewCell,UIWebViewDelegate {
    
    @IBOutlet weak var webKit: UIWebView!
    @IBOutlet weak var vwBG: UIView!
    @IBOutlet weak var lblRounderOption: UILabel!
  //  @IBOutlet weak var lblOption: UILabel!
    @IBOutlet weak var optionheight: NSLayoutConstraint!
    
    @IBOutlet weak var optionwebheight: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
// class TestMainCell: UICollectionViewCell
//  {
//
//    @IBOutlet weak var webkit: UIWebView!
//    @IBOutlet weak var lblTile: UILabel!
//    @IBOutlet weak var tblList: UITableView!
//    var finishedLoadingInitialTableCells = false
//    var dataToOptions: [OptionsList] = []
//    var arr_selected = [[String : String]]()
//    var arr_Options = [[String : Any]]()
//    var currentIndex = Int()
//
//    @IBOutlet weak var constWebviewHeight: NSLayoutConstraint!
//    weak var cellDelegate: SubCategoryProductDelegate?
//
//    override func awakeFromNib() {
//        super.awakeFromNib()
//
//    }

    /*
     - (void)webViewDidFinishLoad:(UIWebView *)webView
     {
     [tableView beginUpdates];
     [tableView endUpdates];
     }
  */
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        self.layoutIfNeeded()
//        constWebviewHeight.constant = webkit.scrollView.contentSize.height
//       // constWebviewHeight.constant = webkit.collectionViewLayout.collectionViewContentSize.height
//         self.layoutIfNeeded()
//    }

    //MARK:- Webview Delegate
    
    
//    func webViewDidFinishLoad(_ webView: UIWebView) {
//        self.layoutIfNeeded()
//        var wRect = webView.frame
//        wRect.size.height = webView.scrollView.contentSize.height
//        webView.frame = wRect
//        constWebviewHeight.constant = webView.scrollView.contentSize.height
//        print(webView.scrollView.contentSize.height)
//        self.layoutIfNeeded()
//    }

    

//class TestQuestionListCell: UITableViewCell {
//
//
//
//    override func awakeFromNib() {
//        super.awakeFromNib()
//    }
//
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//    }
//}
//}
class TestQuestionListView: UIViewController  , UICollectionViewDelegateFlowLayout , SubCategoryProductDelegate,UIWebViewDelegate, UITableViewDelegate , UITableViewDataSource
{
    
    
    
    @IBOutlet weak var webview: UIWebView!
    
    @IBOutlet weak var imgGredient: UIImageView!
    @IBOutlet weak var imgBG: UIImageView!
 
    @IBOutlet weak var labelHeader: UILabel!
    
    @IBOutlet weak var videoslider: UISlider!
    @IBOutlet weak var bgview: UIView!
    @IBOutlet weak var videoview: UIView!
    @IBOutlet weak var starttime: UILabel!
    @IBOutlet weak var endtime: UILabel!
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var webkit: UIWebView!
   
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var btnrewatch: UIButton!
    @IBOutlet weak var btncontinue: UIButton!
    
    //  var player: AVPlayer!
    var avpController = AVPlayerViewController()
    var isfromsummary = false
    var finishedLoadingInitialCollectionCells = false
    var Question_Paper_List = [[String : Any]]()
    var VIDEO_Question_Paper_List = [[String : Any]]()
    var markerarr:NSMutableArray = []
    var markerarrsection:NSMutableArray = []
    var indexarr:NSMutableArray = []

     var seconds :Float64 = 0

    var Get_PaperID = ""
    var strSubject = String()
    var saveddata = [[String : Any]]()

    var dataToQuestion: [QuestionList] = []
    var dataToOptions: [OptionsList] = []
    
    var Detail_array = [[String : Any]]()
    var arr_selected_Value = [[String : Any]]()

    //MARK:- View Life Cycle
    var timer = Timer()
    var player:AVPlayer?
    var playerItem:AVPlayerItem?
    var playButton:UIButton?
    var imgv:UIImageView?
    var timeObserver: Any?

    @IBOutlet weak var playbackSlider: JMMarkSlider!
    
   // var  playbackSlider = JMMarkSlider()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       btnrewatch.layer.borderColor = #colorLiteral(red: 0.9450980392, green: 0.3294117647, blue: 0.431372549, alpha: 1)
        btnrewatch.layer.borderWidth = 1.0
        cellDelegate = self
                tblList.delegate = self
                tblList.dataSource = self
                webkit.delegate = self
        scrollview.isHidden = true
        webview.delegate = self
        let weburl = ServiceList.WEBVIEW_URL
        let userid = (UserDefaults.standard.getUserDict()["student"] as AnyObject).value(forKey: "id") as? String ?? ""
        //let url = WEBVIEW_URL+
        
        let first = weburl + "mcqtest/start_test?UserID=" + userid + "&PaperID=" + Get_PaperID
        
        let myURL = URL(string: first)
        let myRequest = URLRequest(url: myURL!)
        webview.loadRequest(myRequest)
        print("PaperID: \(Get_PaperID)")
      
        imgBG.setGredient()
        imgGredient.setGredient()
        labelHeader.text = strSubject
        let strHTML = "<!DOCTYPE html>\n" +
            "<html>\n" +
            "<head>\n" +
            "<title>MathJax MathML Test Page</title>\n" +
            
            "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n" +
            "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\n" +
            "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
            "\n" +
            "\n" +
            "<link rel=\"stylesheet\" href=\"file:///android_asset/mathscribe/jqmath-0.4.3.css\">" +
            "\n" +
            "<script type=\"text/javascript\" src=\"file:///android_asset/mathscribe/jquery-1.4.3.min.js\"></script>\n" +
            "\n" +
            "<script type=\"text/javascript\" src=\"file:///android_asset/mathscribe/jqmath-etc-0.4.6.min.js\"></script>\n" +
            "\n" +
            "<style> body{} </style>" +
            "\n" +
            "</head>\n" +
            "<body style='font-family:'arial';font-size:30px;color:#FFFFFF; vertical-align:middle;'> \n" +
            "\n" +
            
//        "<p style='color: #FFFFFF'>"; +
//                String postFixHtml = "</p>\n"
        
//        "<p style='color: #FFFFFF'>";
//        String postFixHtml = "</p>\n"
            
            
            "</body>\n" +
        "</html>";
        
        let type = saveddata[0]["Type"] as! String
        if type == "1"
        {
            if self.isfromsummary
            {
                self.scrollview.isHidden = true
                self.setsimplevideo()
            }
            else
            {
               // self.scrollview.isHidden = false
                Video_Question_Paper_List_Api()
            }
            
        }
        else
        {
            videoview.isHidden = true
            Question_Paper_List_Api()
        }
        
        
        
    }
    
    func setsimplevideo()
    {
        let url = URL(string: saveddata[0]["VideoURL"] as! String)
        let playerItem:AVPlayerItem = AVPlayerItem(url: url!)
        player = AVPlayer(playerItem: playerItem)
        
        let playerLayer=AVPlayerLayer(player: player!)
        playerLayer.frame=CGRect(x:0, y:0, width:self.videoview.frame.size.width, height:self.videoview.frame.size.height-100)
        imgv = UIImageView(frame: playerLayer.frame)
        if let url  = NSURL(string: saveddata[0]["VideoImage"] as! String),
            let data = NSData(contentsOf: url as URL)
        {
            imgv?.image = UIImage(data: data as Data)
        }
        self.videoview.addSubview(imgv!)
        self.videoview.layer.addSublayer(playerLayer)
        
        self.player?.seek(to: CMTimeMakeWithSeconds((Float64(Float((self.saveddata[0]["StartTime"] as? NSString)!.doubleValue )/1000)), preferredTimescale: 1), toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
        
        playButton = UIButton(type: UIButton.ButtonType.system) as UIButton
        let xPostion:CGFloat = 10
        let yPostion:CGFloat = self.videoview.frame.size.height-80
        let buttonWidth:CGFloat = 40
        let buttonHeight:CGFloat = 40
        
        playButton!.frame = CGRect(x:xPostion, y:yPostion, width:buttonWidth, height:buttonHeight)
        playButton?.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        // playButton!.backgroundColor = UIColor.lightGray
        playButton!.setImage(UIImage(named: "play"), for: .normal)
        // playButton!.setTitle("Play", for: UIControl.State.normal)
        playButton!.tintColor = UIColor.black
        //playButton!.addTarget(self, action: Selector("playButtonTapped:"), for: .touchUpInside)
        playButton!.addTarget(self, action: #selector(TestQuestionListView.playButtonTapped(_:)), for: .touchUpInside)
         self.videoview.addSubview(playButton!)
        seconds  = ((self.saveddata[0]["EndTime"] as? NSString)!.doubleValue - (self.saveddata[0]["StartTime"] as? NSString)!.doubleValue)/1000      //CMTimeGetSeconds(duration)
        self.endtime?.text = self.stringFromTimeInterval(interval: seconds)
        videoslider.maximumValue =   Float(seconds)
        videoslider.minimumValue =  0
        
        videoview.bringSubviewToFront(starttime)
        videoview.bringSubviewToFront(endtime)
        videoview.bringSubviewToFront(imgv!)
        
        videoslider.setThumbImage(UIImage(named: "thumb"), for: .normal)
        
        NotificationCenter.default.addObserver(self, selector: #selector(TestQuestionListView.finishVideo), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        
        timeObserver = player!.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: nil) { (CmTime) -> Void in
            if self.player!.currentItem?.status == .readyToPlay {
                let time : Float64 = CMTimeGetSeconds(self.player!.currentTime());
                self.videoslider.value = Float ( time - Float64((self.saveddata[0]["StartTime"] as? NSString)!.doubleValue)/1000 );
                self.starttime?.text = self.stringFromTimeInterval(interval: (time - Float64((self.saveddata[0]["StartTime"] as? NSString)!.doubleValue)/1000))
                let timestr = String(format: "%.0f", (time))
                print("TIMERUNNING:",timestr)
              //  let endtime = String(((self.saveddata[0]["EndTime"] as? NSString)!.doubleValue)/1000)
               let endtime = String(format: "%.0f", (((self.saveddata[0]["EndTime"] as? NSString)!.doubleValue)/1000))
                if timestr == endtime
                {
                     self.player?.seek(to: CMTimeMakeWithSeconds((Float64(Float((self.saveddata[0]["StartTime"] as? NSString)!.doubleValue )/1000)), preferredTimescale: 1), toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
                    self.playButtonTapped(self.playButton!)
                    self.imgv?.isHidden = false
                    self.videoview.bringSubviewToFront(self.imgv!)
                }
                
            }
        }
    }
    
    func setslider()
    {
        let type = saveddata[0]["Type"] as! String
        if type == "1"
        {
            webview.delegate = nil
            
            webview.isHidden = true
            webview.backgroundColor = UIColor.clear
            bgview.backgroundColor = UIColor.clear
            //            let url = URL(string:saveddata[0]["VideoURL"] as! String)
            //            player = AVPlayer(url: url!)
            //            avpController.player = player
            //            avpController.view.frame.size.height = videoview.frame.size.height
            //            avpController.view.frame.size.width = videoview.frame.size.width
            //            self.videoview.addSubview(avpController.view)
            
            
            
            let url = URL(string: saveddata[0]["VideoURL"] as! String)
            let playerItem:AVPlayerItem = AVPlayerItem(url: url!)
            player = AVPlayer(playerItem: playerItem)
            
            let playerLayer=AVPlayerLayer(player: player!)
            playerLayer.frame=CGRect(x:0, y:0, width:self.videoview.frame.size.width, height:self.videoview.frame.size.height-100)
            imgv = UIImageView(frame: playerLayer.frame)
            if let url  = NSURL(string: saveddata[0]["VideoImage"] as! String),
                let data = NSData(contentsOf: url as URL)
            {
                imgv?.image = UIImage(data: data as Data)
            }
            self.videoview.addSubview(imgv!)
            self.videoview.layer.addSublayer(playerLayer)
            

            self.player?.seek(to: CMTimeMakeWithSeconds((Float64(Float((self.saveddata[0]["StartTime"] as? Double)!)/1000)), preferredTimescale: 1), toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
       
            playButton = UIButton(type: UIButton.ButtonType.system) as UIButton
            let xPostion:CGFloat = 10
            let yPostion:CGFloat = self.videoview.frame.size.height-80
            let buttonWidth:CGFloat = 40
            let buttonHeight:CGFloat = 40
            
            playButton!.frame = CGRect(x:xPostion, y:yPostion, width:buttonWidth, height:buttonHeight)
            playButton?.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            // playButton!.backgroundColor = UIColor.lightGray
            playButton!.setImage(UIImage(named: "play"), for: .normal)
           // playButton!.setTitle("Play", for: UIControl.State.normal)
            playButton!.tintColor = UIColor.black
            //playButton!.addTarget(self, action: Selector("playButtonTapped:"), for: .touchUpInside)
            playButton!.addTarget(self, action: #selector(TestQuestionListView.playButtonTapped(_:)), for: .touchUpInside)
            
            self.videoview.addSubview(playButton!)
            for i in 0 ..< markerarr.count
            {
                let  slider = UISlider(frame:CGRect(x:videoslider.frame.origin.x, y:self.videoslider.frame.origin.y+15, width:self.videoslider.frame.size.width, height:30))
                
                slider.minimumValue = 0
                slider.maximumValue = Float((self.saveddata[0]["EndTime"] as? Double)!) - Float((self.saveddata[0]["StartTime"] as? Double)!)
                slider.value = Float(markerarr.object(at: i) as! Double) - Float((self.saveddata[0]["StartTime"] as? Double)!)
                slider.maximumTrackTintColor = UIColor.clear
                slider.minimumTrackTintColor = UIColor.clear
                slider.backgroundColor = UIColor.clear
                slider.setThumbImage(UIImage(named: "marker"), for: .normal)
                self.videoview.addSubview(slider)
            }
            
            // Add playback slider
            
          //  playbackSlider = JMMarkSlider(frame:CGRect(x:videoslider.frame.origin.x, y:self.videoslider.frame.origin.y+20, width:self.videoslider.frame.size.width, height:30))
//            playbackSlider.minimumValue = 0
//
          

//            self.endtime?.text = self.stringFromTimeInterval(interval: seconds)
//            playbackSlider.maximumValue = Float(seconds)
//
//            playbackSlider.isContinuous = true
//           // playbackSlider.tintColor = UIColor.green
//            playbackSlider.selectedBarColor = UIColor.clear
//            playbackSlider.unselectedBarColor = UIColor.clear
//            playbackSlider.thumbTintColor = UIColor.clear
////            let leftTrackImage = UIImage(named: "Cross")
////            playbackSlider.setThumbImage(leftTrackImage, for: .normal)
//             playbackSlider.markPositions = markerarr as? [Any]
//          //  self.playbackSlider.markColor = UIColor.init(patternImage: UIImage(named: "marker")!)
//             self.playbackSlider.markColor = UIColor.red
//                      //  self.playbackSlider.markPositions = self.markerarr as? [Double]
//                      //  self.playbackSlider.markPositions = [15,36,77]
//                        self.playbackSlider.markWidth = 5
//            playbackSlider.isUserInteractionEnabled = false
//           // playbackSlider.addTarget(self, action: #selector(self.handlePlayheadSliderValueChanged), for: .valueChanged)
//           //  playbackSlider.addTarget(self, action: "playbackSliderValueChanged:", forControlEvents: .ValueChanged)
//            self.videoview.addSubview(playbackSlider)
            let duration : CMTime = playerItem.asset.duration
            seconds  = ((self.saveddata[0]["EndTime"] as? Double)! - (self.saveddata[0]["StartTime"] as? Double)!)/1000      //CMTimeGetSeconds(duration)
            self.endtime?.text = self.stringFromTimeInterval(interval: seconds)
            videoslider.maximumValue =   Float(seconds)
            videoslider.minimumValue =  0

            videoview.bringSubviewToFront(starttime)
            videoview.bringSubviewToFront(endtime)
            videoview.bringSubviewToFront(imgv!)

            videoslider.setThumbImage(UIImage(named: "thumb"), for: .normal)
            
            NotificationCenter.default.addObserver(self, selector: #selector(TestQuestionListView.finishVideo), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)

//        timeObserver = player!.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: nil) { (CMTime) -> Void in
//                if self.player!.currentItem?.status == .readyToPlay {
//                    let time : Float64 = CMTimeGetSeconds(self.player!.currentTime());
//                    self.videoslider.value = Float ( time );
//                    self.starttime?.text = self.stringFromTimeInterval(interval: time)
//                    let timestr = String(format: "%.0f", time)
//                    print("TIMERUNNING:",timestr)
//                    if self.markerarrsection.contains(timestr)
//                    {
//                        self.currentIndex = self.markerarrsection.index(of: timestr)
//                        print("INDEX:",self.currentIndex)
//                        if self.indexarr.contains(self.markerarrsection.object(at: self.currentIndex))
//                        {
//                        }
//                        else
//                        {
//                            self.indexarr.add(self.markerarrsection.object(at: self.currentIndex))
//                            self.settabledata()
//                        }
//                       // self.player?.removeTimeObserver(self.timeObserver!)
//                    }
//                    else
//                    {
//
//                    }
//                }
//            }
            timeObserver = player!.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: nil) { (CmTime) -> Void in
                if self.player!.currentItem?.status == .readyToPlay {
                    let time : Float64 = CMTimeGetSeconds(self.player!.currentTime());
                    self.videoslider.value = Float ( time - Float64((self.saveddata[0]["StartTime"] as? Double ?? 0))/1000 );
                    self.starttime?.text = self.stringFromTimeInterval(interval: (time - Float64((self.saveddata[0]["StartTime"] as? Double ?? 0))/1000))
                    let timestr = String(format: "%.0f", (time))
                    print("TIMERUNNING:",timestr)
                    let endtime = String(format: "%.0f", ((((self.saveddata[0]["EndTime"] as? Double)!))/1000))
                    if timestr == endtime
                    {
                        self.player?.seek(to: CMTimeMakeWithSeconds((Float64(Float(((self.saveddata[0]["StartTime"] as? Double)!) )/1000)), preferredTimescale: 1), toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
                        self.playButtonTapped(self.playButton!)
                        self.imgv?.isHidden = false
                        self.videoview.bringSubviewToFront(self.imgv!)
                       
                        self.finishVideo()
                    }
                   
                    if self.markerarrsection.contains(timestr)
                    {
                        self.currentIndex = self.markerarrsection.index(of: timestr)
                        print("INDEX:",self.currentIndex)
                        if self.indexarr.contains(self.markerarrsection.object(at: self.currentIndex))
                        {
                        }
                        else
                        {
                            self.indexarr.add(self.markerarrsection.object(at: self.currentIndex))
                            self.settabledata()
                        }
                        // self.player?.removeTimeObserver(self.timeObserver!)
                    }
                    else
                    {
                        
                    }
                }
            }
        }
        else
        {
         //   videoview.isHidden = true
          //  Question_Paper_List_Api()
        }
        //------------------------------- VIDEO PLAYER CONFIGURATION ------------------------------------
    }
    @objc func finishVideo()
    {
        print("Video Finished")
        let alert = UIAlertController(title: "", message: "Your time should be complete.Please click on OK to see your reports.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            print("Okay'd")
            self.Student_Submit_Test_Api()
        }))
        self.present(alert, animated: true, completion: nil)
    }

    func settabledata()
    {
       
        //DispatchQueue.main.async {
            self.playButtonTapped(self.playButton!)
            self.scrollview.isHidden = false
            self.arr_Options = self.VIDEO_Question_Paper_List[self.currentIndex]["Options"] as? [[String : Any]]  ?? []
            print("OPTIONARR:",self.arr_Options.count)
            let optiondata = self.VIDEO_Question_Paper_List[self.currentIndex]["Question"] as? String ?? ""
            self.webkit.loadHTMLString(optiondata , baseURL: nil)
            self.tblList!.reloadData()
        if Thread.isMainThread {
            print("Main Thread")
        }
        else
        {
            print("Background Thread")
            
        }
      //  }
    }
    
    func stringFromTimeInterval(interval: TimeInterval) -> String {
        
        let interval = Int(interval)
        let seconds = interval % 60
        let minutes = (interval / 60) % 60
        let hours = (interval / 3600)
       
        if (hours > 0) {
            return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
        } else {
            return String(format: "%02d:%02d", minutes, seconds)
        }
    }
    
//  @IBAction func handlePlayheadSliderValueChanged(_ sender: UISlider)
//    {
//
//        let seconds : Int64 = Int64(sender.value)
//        let targetTime:CMTime = CMTimeMake(value: seconds, timescale: 1)
//
//        player!.seek(to: targetTime)
//
//        if player!.rate == 0
//        {
//            player?.play()
//        }
//
//
//    }
    
    
    @objc func playButtonTapped(_ sender:UIButton)
    {
        
     //   DispatchQueue.main.async {
            self.imgv?.isHidden = true
            if self.player?.rate == 0
            {
                self.player!.play()
                self.playButton!.setImage(UIImage(named: "pause"), for: .normal)
                
                //  playButton!.setTitle("Pause", for: UIControl.State.normal)
            } else {
                self.player!.pause()
                self.playButton!.setImage(UIImage(named: "play"), for: .normal)
                //  playButton!.setTitle("Play", for: UIControl.State.normal)
            }
      //  }
        
    }
    
    
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if let text = webView.request?.url?.absoluteString{
            print(text)
        }
        
        var frame = webkit.frame
        
        frame.size.width = self.webkit.frame.size.width
        frame.size.height = 1
        
        webkit.frame = frame
        frame.size.height = webkit.scrollView.contentSize.height
//        if frame.size.height < 65 {
//            frame.size.height = 65
//        }
//        else
//        {
//            frame.size.height = webkit.scrollView.contentSize.height
//        }
        webkit.frame = frame;
     //   constWebviewHeight.constant = webkit.scrollView.contentSize.height + 50
        print("TOPWEBHEIGHT:",frame)
        //  self.layoutIfNeeded()
        tblList.frame.origin.y = webkit.scrollView.contentSize.height + 10
        print("TBLFRAME:",tblList.frame)
       // webView.resizeWebContent()
        
        //            webView.frame.size.height = 1;
        //            webView.frame.size = webView.sizeThatFits(.zero)
        
        
//        if (contentHeights[webView.tag] != 0.0)
//        {
//            // we already know height, no need to reload cell
//            return
//        }
//        print("CELLHEIGGHT:",webView.scrollView.contentSize.height as Any)
//        if  webView.scrollView.contentSize.height > 56
//        {
//            contentHeights[webView.tag] = webView.scrollView.contentSize.height + 20
//        }
//        else
//        {
//            contentHeights[webView.tag] = webView.frame.size.height + 20
//        }
//
//        tblList.reloadRows(at: [IndexPath(row: webView.tag, section: 0)], with: .automatic)
        loadViewIfNeeded()
        
    }
    
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        
        let url = "\(request.url)"
        let weburl = ServiceList.WEBVIEW_URL
        let first = weburl + "app_redirect?id="
        
        if Int((url as NSString?)?.range(of: first).location ?? 0) == NSNotFound {
           print("\(url)")
        } else {
            if let url = request.url {
          
                let dirString:String=url.absoluteString
                let items = dirString.components(separatedBy: "=")
                let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "TestReportView") as! TestReportView
                
                secondVc.PaperID = items[1]//self.Get_PaperID
                self.navigationController?.pushViewController(secondVc, animated: true)
            }
        }
        
        // you can call any thing on click.
        
        
        return true
    }
    //MARK:- WEB SERVICES
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var pageIndex = Int(scrollView.contentOffset.x/view.frame.width)
        pageIndex = pageIndex+1
        
        print("pageIndex => ",pageIndex)
        print("self.Question_Paper_List.count => ",self.Question_Paper_List.count)
        
        var String1 = String(pageIndex)
        String1 += "/"
        String1 += String(self.Question_Paper_List.count)
   
        
        //self.pageControl.currentPage = Int(pageIndex)
        
    }
    
    func Question_Paper_List_Api()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let parameters = ["PaperID" : Get_PaperID] as [String : Any]
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["student"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.GET_PAPER_QUESTION_LIST)!,
                              method: .post,
                              parameters: parameters,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                return
                        }
                        
                        resData = data
                        if resData["status"] as? Bool ?? false
                        {
                 self.Question_Paper_List = resData["data"] as? [[String : Any]]  ?? []
                                                }
                        
                      
                        
                       // showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    func Video_Question_Paper_List_Api()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let parameters = ["PaperID" : Get_PaperID] as [String : Any]
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["student"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.GET_VIDEO_PAPER_QUESTION_LIST)!,
                              method: .post,
                              parameters: parameters,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                return
                        }
                        
                        resData = data
                        if resData["status"] as? Bool ?? false
                        {
                            self.VIDEO_Question_Paper_List = resData["data"] as? [[String : Any]]  ?? []
                            
                            for post in self.VIDEO_Question_Paper_List
                            {
                                 let QuestionMarkers = post["QuestionMarkers"] as? Double
                                //self.markerarr.adding(QuestionMarkers!)
                               // let addmark = Double(QuestionMarkers!*100) / Double((self.saveddata[0]["EndTime"] as? Double)!)
                                self.markerarr.add(QuestionMarkers!)
                                let timestr = String(format: "%.0f", QuestionMarkers!/1000)
                                self.markerarrsection.add(timestr)
                            }
                            SVProgressHUD.dismiss()
                             self.setslider()
                            
                        }
                        
                        // showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    func Student_Submit_Test_Api()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            var convertedString = String()
            do {
                let data1 =  try JSONSerialization.data(withJSONObject: arr_selected_Value, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
                convertedString = String(data: data1, encoding: String.Encoding.utf8)! // the data will be converted to the string
                print(convertedString) // <-- here is ur string
                
            } catch let myJSONError {
                print(myJSONError)
            }
            
            let parameters = ["PaperID" : Get_PaperID ,
                              "detail_array" : convertedString, "TakenTime" : 0] as [String : Any]
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["student"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.STUDENT_SUBMIT_TEST)!,
                              method: .post,
                              parameters: parameters,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                return
                        }
                        
                        resData = data
                        if resData["status"] as? Bool ?? false
                        {
                            let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "TestReportView") as! TestReportView
                            secondVc.PaperID = self.Get_PaperID
                            self.navigationController?.pushViewController(secondVc, animated: true)
                        }
                        
                        showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    
    //MARK:- View Life Cycle
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnEndTestAction(_ sender: Any) {
        
        Student_Submit_Test_Api()
        
//        let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "TestReportView") as! TestReportView
//        secondVc.PaperID = Get_PaperID
//        self.navigationController?.pushViewController(secondVc, animated: true)
    }
    
    //MARK:- Collection view methods
    
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 1
//    }
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return Question_Paper_List.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TestMainCell", for: indexPath) as! TestMainCell
//        cell.layoutIfNeeded()
//        cell.currentIndex = indexPath.row
//        cell.cellDelegate = self
//
////        cell.frame = tableView.bounds;
////        [cell layoutIfNeeded];
////        [cell.collectionView reloadData];
////        cell.collectionViewHeight.constant = cell.collectionView.collectionViewLayout.collectionViewContentSize.height;
//
//
//        cell.webkit.loadHTMLString((Question_Paper_List[indexPath.row]["Question"] as? String ?? ""), baseURL: nil)
//
//       // cell.constWebviewHeight.constant = cell.webkit.collectionViewLayout.collectionViewContentSize.height
//
//         cell.webkit.isOpaque = false
//         cell.webkit.backgroundColor = UIColor.clear
//
////         let value = Question_Paper_List[indexPath.row]["Options"] as? [[String : Any]]  ?? []
////         cell.dataToOptions.removeAll()
//
//        cell.arr_Options.removeAll()
//        cell.arr_Options = Question_Paper_List[indexPath.row]["Options"] as? [[String : Any]]  ?? []
//
////          for i in 0..<value.count {
////            cell.arr_Options.append(value[i])
////              let options = value[i]["Options"] as? String ?? ""
////            cell.dataToOptions.append(OptionsList(Options: options.htmlToString))
////          }
//
//        cell.tblList.reloadData()
//
//        cell.layoutIfNeeded()
//        return cell
//    }
//
//
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
//    {
//        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
//    }
//
//    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//
//        var lastInitialDisplayableCell = false
//        if 5 > 0 && !finishedLoadingInitialCollectionCells {
//            if let indexPathsForVisibleRows = collectionView.indexPathsForVisibleItems.last,
//                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath == indexPath.row {
//                lastInitialDisplayableCell = true
//            }
//        }
//
//        if !finishedLoadingInitialCollectionCells {
//            if lastInitialDisplayableCell {
//                finishedLoadingInitialCollectionCells = true
//            }
//            cell.loadAnimationForCollection()
//        }
//    }
    
    func didPressButton(arr: [String : Any] , index : Int) {
        let Ids = arr_selected_Value.map({ $0["MCQPlannerDetailsID"] as? String ?? ""})
        if Ids.contains(arr["MCQPlannerDetailsID"] as? String ?? "")
        {
           // arr_selected_Value.remove(at: index)
            arr_selected_Value.removeLast()
        }
        
        arr_selected_Value.append(arr)
        print(arr_selected_Value)
      //  arr_selected_Value.insert(arr, at: index)
    }
    
    
    //----------------------------------- QUESTION OPTION VIEW CONFIG ---------------------------------------------
    
    var finishedLoadingInitialTableCells = false
   // var dataToOptions: [OptionsList] = []
    var arr_selected = [[String : String]]()
    var arr_Options = [[String : Any]]()
    var currentIndex:Int = 0
    weak var cellDelegate: SubCategoryProductDelegate?

    //@IBOutlet weak var constWebviewHeight: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
//        tblList.delegate = self
//        tblList.dataSource = self
//        webkit.delegate = self
        //        self.tblList.estimatedRowHeight = 150
        //        self.tblList.rowHeight = UITableView.automaticDimension
    }
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        self.layoutIfNeeded()
//        constWebviewHeight.constant = webkit.scrollView.contentSize.height
//
//        self.layoutIfNeeded()
//    }
    func insertCSSString(into webView: UIWebView) {
        let cssString = "body { width:100% !important; font-size: 15px; color: #000 } table { width:100% !important; color: #000; border-color: #000; border: black; border-collapse: collapse;}"
        let jsString = "var style = document.createElement('style'); style.innerHTML = '\(cssString)'; document.head.appendChild(style);"
        webView.stringByEvaluatingJavaScript(from: jsString)
    }
    
    var contentHeights : [CGFloat] = [0.0,0.0,0.0,0.0,0.0]
    
    
    //MARK:- Tableview methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_Options.count
    }
    var cellindex:NSMutableArray = []
    // var cell = TestQuestionListCell()
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:TestQuestionListCell = tableView.dequeueReusableCell(withIdentifier: "TestQuestionListCell") as! TestQuestionListCell
        
        cell.layoutIfNeeded()
        
        let htmlHeight = contentHeights[indexPath.row]
        cell.lblRounderOption.setRadius(radius: cell.lblRounderOption.frame.height/2)
        cell.vwBG.setRadius(radius: 10)
        cell.vwBG.setShadow()
        cell.vwBG.layer.borderWidth = 0.5
        cell.vwBG.layer.borderColor = UIColor.lightGray.cgColor
        //  cell.vwBG.setTopShadow()
        cell.webKit.delegate = self
        //  cell.lblRounderOption.text = "\(indexPath.row + 1)"
        cell.lblRounderOption.text = indexPath.row.correspondingLetter(inUppercase: true)
        
        //        cell.webKit.loadHTMLString((dataToOptions[indexPath.row].Options ?? ""), baseURL: nil)
        //  cellindex.insert(1, at: indexPath.row)
        cell.webKit.tag = indexPath.row
        let optiondata = arr_Options[indexPath.row]["Options"] as? String ?? ""
        
        cell.webKit.loadHTMLString(optiondata , baseURL: nil)
        // cell.webKit.scrollView.isScrollEnabled = false
        cell.webKit.isOpaque = false
        cell.webKit.backgroundColor = UIColor.clear
        
        let arr : [String] = arr_selected.map({ $0["AnswerID"] ?? ""})
        if arr.contains(arr_Options[indexPath.row]["MCQOptionID"] as? String ?? "")
        {
            cell.vwBG.backgroundColor = UIColor.init(rgb: ConstantVariables.Constants.lightGreen)
        }
        else
        {
            cell.vwBG.backgroundColor = UIColor.white
        }
        
      //  cell.webKit.frame = CGRect(x: cell.webKit.frame.origin.x, y: 0, width: cell.webKit.frame.size.width, height: htmlHeight)
        //        cell.webKit.frame.size.height = 1
        //        cell.webKit.frame.size = (cell.webKit.sizeThatFits(CGSize.zero))
        
       // cell.optionheight.constant = cell.webKit.scrollView.contentSize.height-10
        //   cell.contentView.frame.size.height = cell.webKit.scrollView.contentSize.height-10
       // cell.optionwebheight.constant = cell.webKit.scrollView.contentSize.height-10
        
        //       cell.setNeedsLayout()
        cell.layoutIfNeeded()
        //cell.updateConstraints()
        // cell.updateConstraintsIfNeeded()
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //    return contentHeights[indexPath.row]
//        if contentHeights[indexPath.row] != 0.0 {
//            //  let cell:TestQuestionListCell = tableView.dequeueReusableCell(withIdentifier: "TestQuestionListCell") as! TestQuestionListCell
//            print("MYHEIGHT:",contentHeights[indexPath.row])
//
//            return contentHeights[indexPath.row]
//        }
//        return 0
        return 75
    }
    //    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    //        return 100
    //    }
    var isselected = false
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isselected = true
        var dict = [String : String]()
        //        dict["IsCorrect"] = arr_Options[indexPath.row]["IsCorrect"] as? String ?? ""
        //        dict["MCQOptionID"] = arr_Options[indexPath.row]["MCQOptionID"] as? String ?? ""
        //        dict["MCQQuestionID"] = arr_Options[indexPath.row]["MCQQuestionID"] as? String ?? ""
        
        //        [{"AnswerID":0,"IsAttempt":"0","MCQPlannerDetailsID":3}]
        dict["AnswerID"] = arr_Options[indexPath.row]["MCQOptionID"] as? String ?? ""
        dict["IsAttempt"] = "1"
        dict["MCQPlannerDetailsID"] = arr_Options[indexPath.row]["MCQQuestionID"] as? String ?? ""
        
        arr_selected.removeAll()
        arr_selected.append(dict)
        
        tblList.reloadData()
        
        cellDelegate?.didPressButton(arr: dict, index: currentIndex)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var lastInitialDisplayableCell = false
        if 4 > 0 && !finishedLoadingInitialTableCells {
            if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        
        if !finishedLoadingInitialTableCells {
            if lastInitialDisplayableCell {
                finishedLoadingInitialTableCells = true
            }
            cell.loadAnimationForCollection()
        }
    }
    @IBAction func rewatch_click(_ sender: UIButton) {
     //   DispatchQueue.main.async {
        if Thread.isMainThread {
            print("Main Thread")
        }
        else
        {
            print("Background Thread")

        }
       // player?.removeTimeObserver(timeObserver!)
            self.scrollview.isHidden = true
            let frameRate : Int32 = (self.player?.currentItem?.currentTime().timescale)!
        var secondo = Float64()
        if indexarr.count>1
        {
            secondo = Float64(self.indexarr.object(at: self.indexarr.count-2) as! String )!
        }
        else
        {
            secondo = 0
        }
        self.isselected = false
      //  player!.currentItem!.seekToTime(CMTimeMakeWithSeconds(secondo, frameRate), toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
            self.player?.currentItem?.seek(to: CMTimeMakeWithSeconds(secondo, preferredTimescale: frameRate), toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
      //  }
       indexarr.removeLastObject()
        self.playButtonTapped(self.playButton!)
    }
    @IBAction func continue_click(_ sender: UIButton) {
      //  DispatchQueue.main.async {
            if self.isselected
            {
                self.isselected = false
                self.scrollview.isHidden = true
                self.playButtonTapped(self.playButton!)
            }
            else
            {
                showToast(uiview: self, msg: "Please select answer")
            }
       // }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        player?.pause()
    }
}

extension Int {
    func correspondingLetter(inUppercase uppercase: Bool = false) -> String? {
        let firstLetter = uppercase ? "A" : "a"
        let startingValue = Int(UnicodeScalar(firstLetter)!.value)
        if let scalar = UnicodeScalar(self + startingValue) {
            return String(scalar)
        }
        return nil
    }
}

extension Array where Element: Equatable {
    func indexes(of element: Element) -> [Int] {
        return self.enumerated().filter({ element == $0.element }).map({ $0.offset })
    }
}

//
//  OTPVerificationView.swift
//  TeacherApp
//
//  Created by archana on 23/01/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import SVProgressHUD

class OTPVerificationView: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var imgGredient: UIImageView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var txtOTP: UITextField!
    @IBOutlet weak var btnVerify: UIButton!
    @IBOutlet weak var lblMobileNo: UILabel!
    
    @IBOutlet weak var imgVerifyBG: UIImageView!
    
    var otpGet = 0
    var mobilenoGet="0000000000";
    var StudentCodeGet = ""
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imgGredient.setGredient()
        btnVerify.setRadius(radius: btnVerify.frame.height/2)
        imgLogo.setRadius(radius: imgLogo.frame.height/2)
        btnVerify.dampAnimation()
        imgVerifyBG.setRadius(radius: imgVerifyBG.frame.height/2)
        btnVerify.setGredientText()
        
        let trimmedString: String = (mobilenoGet as NSString).substring(from: 6);
        
        lblMobileNo.text="OTP has been sent to your mobile number XXXXXX"+trimmedString;
    }
    
    //MARK:- WEB SERVICES
    
    func resendApi()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            SVProgressHUD.show(withStatus: nil)
            let DeviceID = UIDevice.current.identifierForVendor?.uuidString
            let parameters = ["StudentCode" : StudentCodeGet,
                              "DeviceID": DeviceID!] as [String : Any]
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.RESEND_OTP)!,
                              method: .post,
                              parameters: parameters,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        if resData["status"] as? Bool ?? false
                        {
                            var dict = resData["data"] as? [String : Any] ?? [:]
                            let otp = dict["otp"] as? Int ?? 0
                            print(" otp is: \(otp)")
                           
                        }
                        
                        showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    
    func ConfirmApi()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            SVProgressHUD.show(withStatus: nil)
            let DeviceID = UIDevice.current.identifierForVendor?.uuidString
          
            let parameters = ["StudentCode" : StudentCodeGet,
                              "OTP" : txtOTP.text!,
                              "PlayerID" : "1" ,
                              "DeviceID": DeviceID!] as [String : Any]
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.CONFIRM_OTP)!,
                              method: .post,
                              parameters: parameters,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        if resData["status"] as? Bool ?? false
                        {
                            
                            var dict = resData["data"] as? [String : Any] ?? [:]
                            
                            for (key, value) in dict {
                                let val : NSObject = value as! NSObject;
                                dict[key] = val.isEqual(NSNull()) ? "" : value
                            }
                            
                            print(dict)
                            UserDefaults.standard.setUserDict(value: dict)
                            UserDefaults.standard.setIsLogin(value: true)
                            
                            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
                            self.navigationController?.pushViewController(nextViewController, animated: true)
                            
                            /*
                             ["student": {
                             email = "student1@gmail.com";
                             firstname = Students;
                             id = 13;
                             lastname = Bachani;
                             mobileno = 1234567890;
                             otp = "{\"asdf\":842954,\"3FB4B84F-812F-4FF0-BF3B-6C477C8E2E6E\":350576}";
                             }, "login_token": coc4c40gk80oc0ko4socksw0ck0o0og4]
                             
                             
                             
                             for controller in self.navigationController!.viewControllers as Array {
                             if controller.isKind(of: DashboardViewController.self) {
                             _ = self.navigationController!.popToViewController(controller, animated: true)
                             break
                             }
                             }
                             
                             var dict = resData["data"] as? [String : Any] ?? [:]
                             
                             var dic1 = dict["teacher"] as? [String : Any] ?? [:]
                             
                             let id = dic1["id"] as? String ?? ""
                             let firstname = dic1["firstname"] as? String ?? ""
                             let lastname = dic1["lastname"] as? String ?? ""
                             let username = dic1["username"] as? String ?? ""
                             let email = dic1["email"] as? String ?? ""
                             let mobileno = dic1["mobileno"] as? String ?? ""
                             print("teacher: \(id + " " + firstname + " " + lastname + " " + username + " " + email + " " + mobileno)")
                             
                             */
                            
                        }
                        
                        showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    
    func validation()
    {
        self.view.endEditing(true)
       
        if txtOTP.text == ""
        {
            showAlert(uiview: self, msg: ConstantVariables.LocalizeString.PLEASE_ENTER + " " + ConstantVariables.LocalizeString.STUDENT_CODE, isTwoButton: false)
        }
            
        else {
            resendApi()
        }
    }
    
    func confirmValidation()
    {
        self.view.endEditing(true)
         let textOtp =  Int(txtOTP.text!)
        
        if txtOTP.text == ""
        {
            showAlert(uiview: self, msg: ConstantVariables.LocalizeString.PLEASE_ENTER + " " + ConstantVariables.LocalizeString.OTP, isTwoButton: false)
        }
            
//       else if otpGet != textOtp {
//            showAlert(uiview: self, msg: ConstantVariables.LocalizeString.PLEASE_ENTER + " " + ConstantVariables.LocalizeString.OTP_NOT_MATCH, isTwoButton: false)
//        }
            
        else {
            ConfirmApi()
        }
    }
    
   
     //MARK:- Button Action
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnVerifyAction(_ sender: UIButton) {
          confirmValidation()
//        let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
//        self.navigationController?.pushViewController(secondVc, animated: true)
    }
    
    @IBAction func btnResendAction(_ sender: UIButton) {
       // validation()
        resendApi()
    }
    
    //MARK:- Textfield Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
}



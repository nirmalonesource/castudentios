//
//  DetailReportView.swift
//  Student
//
//  Created by My Mac on 14/09/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class DetailReportCell: UITableViewCell {
    @IBOutlet var lblsubjectname: UILabel!
    @IBOutlet var lblaccuracy: UILabel!
    @IBOutlet var progressvw: UIProgressView!
    @IBOutlet var lblcompleted: UILabel!
    @IBOutlet var lbltotal: UILabel!
    @IBOutlet var lbldate: UILabel!
    @IBOutlet var vwRounder: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}


class DetailReportView: UIViewController, UITableViewDelegate , UITableViewDataSource  {

    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet var lbltitle: UILabel!
    @IBAction func back_click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBOutlet var vwtop: UIView!
    @IBOutlet var btnperformance: UIButton!
    
    @IBOutlet var vwmiddle: UIView!
    @IBOutlet var lblcompleted: UILabel!
    @IBOutlet var lblmissed: UILabel!
    @IBOutlet var lbltotal: UILabel!
    
    @IBOutlet var tblreportlist: UITableView!
    @IBOutlet var tblheight: NSLayoutConstraint!
    
    var finishedLoadingInitialTableCells = false
    var ReportListArr:NSMutableArray = []
    var ReportData:NSMutableDictionary = [:]
    var SubjectID = String()
    var SubjectName = String()
    
    override func viewDidLayoutSubviews() {
        tblheight.constant = tblreportlist.contentSize.height
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        imgBG.setGredient()
        btnperformance.setRadius(radius: btnperformance.frame.size.width/2)
        btnperformance.layer.borderWidth = 3.0
        btnperformance.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
        
        tblreportlist.rowHeight = UITableView.automaticDimension
        tblreportlist.estimatedRowHeight = 600
        vwtop.roundCorners(corners: [.topLeft,.topRight], radius: 10)
        // tblreportlist.roundCorners(corners: [.bottomLeft,.bottomRight], radius: 10)
        lbltitle.text = SubjectName
        TEST_SUMMARY_DETAILS_Api()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ReportListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:DetailReportCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! DetailReportCell
        cell.vwRounder.setRadius(radius: cell.vwRounder.frame.height/2)
        cell.layoutIfNeeded()
        //        cell.vwBG.setShadow()
        
        cell.layoutIfNeeded()
        cell.selectionStyle = .none
        let dic:NSDictionary = ReportListArr.object(at: indexPath.row) as! NSDictionary
        cell.lblsubjectname.text = dic.value(forKey: "SubjectName") as? String
        cell.lblaccuracy.text = String(format: "Accuracy %@%%", dic.value(forKey: "Accuracy") as? String ?? "0") //
        cell.lblcompleted.text = dic.value(forKey: "TotalRight") as? String
        cell.lbltotal.text = dic.value(forKey: "TotalQuestion") as? String
        
        
        let df = dic.value(forKey: "SubmitDate") as? String ?? ""
        if df == "00-00-0000"
        {
            cell.lbldate.text = "Missed"
            cell.lbldate.textColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
        }
        else
        {
            let getDate = setDateWithFormat(strDate: df)
            cell.lbldate.text = getDate
            cell.lbldate.textColor = UIColor.black
        }
        
        
        let OverallPerformance = Float((dic.value(forKey: "Accuracy") as? NSString)?.floatValue ?? 0)
        let floatid = Float(OverallPerformance/100)
        cell.progressvw.setProgress(floatid, animated: true)
        
        
        return cell
    }
    func setDateWithFormat(strDate : String) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d-MMM-yyyy"
        //"MMM dd ,yyyy HH:mm"
        let date: Date? = dateFormatterGet.date(from: strDate)!
        return dateFormatter.string(from: date!)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dic:NSDictionary = ReportListArr.object(at: indexPath.row) as! NSDictionary
        
        let df = dic.value(forKey: "SubmitDate") as? String ?? ""
        if df == "00-00-0000"
        {
            showToast(uiview: self, msg: "Sorry!! Assignment Missed.")
        }
        else
        {
            let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "TestReportView") as! TestReportView
            
            secondVc.PaperID = dic.value(forKey: "StudentMCQTestID") as? String ?? "0"
            self.navigationController?.pushViewController(secondVc, animated: true)
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var lastInitialDisplayableCell = false
        if 3 > 0 && !finishedLoadingInitialTableCells {
            if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        
        if !finishedLoadingInitialTableCells {
            if lastInitialDisplayableCell {
                finishedLoadingInitialTableCells = true
            }
            cell.loadAnimation()
        }
    }
    func setdata()  {
        
        //  btnperformance.setTitle(ReportData.value(forKey: "OverallPerformance") as? String, for: .normal)
        btnperformance.setTitle(String(format: "%@%%", ReportData.value(forKey: "OverallPerformance") as? String ?? ""), for: .normal)
        lbltotal.text = String(format: "Total:-%@", ReportData.value(forKey: "TotalAssign") as? String ?? "")
        lblmissed.text = ReportData.value(forKey: "TotalMissed") as? String
        lblcompleted.text = ReportData.value(forKey: "TotalAttempt") as? String
    }
    
    func TEST_SUMMARY_DETAILS_Api()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
             let parameters = ["SubjectID" : SubjectID] as [String : Any]
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["student"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.GET_SUBJECTSTUDENTPLANNER)!,
                              method: .post,
                              parameters: parameters,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                return
                        }
                        
                        resData = data
                        
                        if resData["status"] as? Bool ?? false
                        {
                            self.ReportData = (resData["data"] as! NSDictionary ).mutableCopy() as! NSMutableDictionary
                            let Missed_Paper_infoArr:NSMutableArray = (self.ReportData.value(forKey: "Missed_Paper_info") as! NSArray).mutableCopy() as! NSMutableArray
                            let Paper_infoArr:NSMutableArray = (self.ReportData.value(forKey: "Paper_info") as! NSArray).mutableCopy() as! NSMutableArray
                            let CombineArr = Paper_infoArr.addingObjects(from: Missed_Paper_infoArr as! [Any])
                            self.ReportListArr = (CombineArr as NSArray).mutableCopy() as! NSMutableArray
                            self.setdata()
                            self.tblreportlist.reloadData()
                        }
                        
                        //  showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
}

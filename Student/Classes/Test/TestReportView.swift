//
//  TestReportView.swift
//  Student
//
//  Created by archana on 05/02/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SDWebImage

class TestReportLevelcolletionCell: UICollectionViewCell {
    
    @IBOutlet weak var lblRadius: UILabel!
    @IBOutlet weak var lblLevelTitle: UILabel!
}

class TestReportTableCell: UITableViewCell
{
    @IBOutlet weak var ProgressView: UIProgressView!
    @IBOutlet weak var AccuracyLabel: UILabel!
    
    @IBOutlet weak var vwBG: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

class TestReportView: UIViewController , UITableViewDelegate , UITableViewDataSource , UICollectionViewDelegate,UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var lblavgtime: UILabel!
    @IBOutlet weak var lbltotaltimetaken: UILabel!
    @IBOutlet weak var imgBGMain: UIImageView!
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var imgTopRadius: UIImageView!
    
    @IBOutlet weak var collectionLevel: UICollectionView!
    @IBOutlet weak var collectionLevelHight: NSLayoutConstraint!
    @IBOutlet weak var collectionLevelTop: NSLayoutConstraint!
    @IBOutlet weak var bottomview: UIView!
    
    
    @IBOutlet weak var tblVW: UITableView!
    @IBOutlet weak var constTblHeight: NSLayoutConstraint!
    @IBOutlet weak var srclVW: UIScrollView!
    
    @IBOutlet weak var btnSubject: UIButton!
    
    @IBOutlet weak var lblScoreCount: UILabel!
    @IBOutlet weak var lblCorrect: UILabel!
    @IBOutlet weak var lblIncorrect: UILabel!
    @IBOutlet weak var lblNotAttempted: UILabel!
    
    var PaperID = ""
    var Test_Report_List = [String : Any]()
    var finishedLoadingInitialTableCells = false
    var finishedLoadingInitialCollectionCells = false

    var test_details1 = [String : Any]()
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        Test_Report_List_Api()
        
        imgBG.setGredient()
        imgBGMain.setGredient()

        imgTopRadius.setRadius(radius: 10)
        imgTopRadius.setShadow()
        
        tblVW.layer.cornerRadius = 5
        tblVW.layer.borderWidth = 1
        tblVW.layer.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        
        bottomview.layer.cornerRadius = 5
        bottomview.layer.borderWidth = 1
        bottomview.layer.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.view.layoutIfNeeded()
        constTblHeight.constant = tblVW.contentSize.height
    }
    override func viewDidLayoutSubviews() {
        srclVW.contentSize = CGSize(width: self.srclVW.frame.size.width, height: 720)
       // self.imgTopRadius.frame.size.height =  self.imgTopRadius.frame.size.height - 50
    }
    //MARK:- WEB SERVICES
    
    func Test_Report_List_Api()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let parameters = ["PaperID" : PaperID] as [String : Any]
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["student"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.STUDENT_TEST_REPORT)!,
                              method: .post,
                              parameters: parameters,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                return
                        }
                        
                        resData = data
                        
                        if resData["status"] as? Bool ?? false
                        {
                            self.Test_Report_List = resData["data"] as? [String : Any]  ?? [:]
                            
                            let option_list = self.Test_Report_List["level_questions"] as? [Any]
                            if option_list?.count == 0
                            {
                                self.collectionLevel.isHidden = true
                                self.collectionLevelHight.constant = 0
                                self.collectionLevelTop.constant = 1
                              //  self.imgTopRadius.frame.size.height =  self.imgTopRadius.frame.size.height - 50
                            }
                            else
                            {
                                self.collectionLevel.isHidden = false
                                self.collectionLevelHight.constant = 120
                                self.collectionLevelTop.constant = 5
                            }
                            
                            
                            if self.Test_Report_List.count > 0
                            {
                                self.setData()
                            }
                        }
                        
                      //  showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }

    func setData()
    {
        let test_details = Test_Report_List["test_details"] as? [String : Any] ?? [:]
        
        test_details1 = Test_Report_List["test_details"] as? [String : Any] ?? [:]
        
        btnSubject.setTitle(test_details["SubjectName"] as? String ?? "", for: .normal)
        lblCorrect.text = "\(test_details["TotalRight"] as? String ?? "")" + " - \(test_details["TotalQuestion"] as? String ?? "")"
        lblIncorrect.text = "\(test_details["TotalWrong"] as? String ?? "")" + " - \(test_details["TotalQuestion"] as? String ?? "")"
        
        lblScoreCount.text = "\(test_details["TotalRight"] as? String ?? "")" + " - \(test_details["TotalQuestion"] as? String ?? "")"
        let Attempt = Int(test_details["TotalAttempt"] as? String ?? "")!
        let total = Int(test_details["TotalQuestion"] as? String ?? "")!
        lblNotAttempted.text = "\(total - Attempt) - " + "\(total)"
        
        let totltime = test_details["TakenTime"] as? String ?? ""
        let avgtime = test_details["AvgTime"] as? String ?? ""

        lbltotaltimetaken.text = stringFromTimeInterval(interval: Double(totltime) ?? 00)
        lblavgtime.text = stringFromTimeInterval(interval: Double(avgtime) ?? 00)
        
        collectionLevel.reloadData()
        tblVW.reloadData()

    }
    
    func stringFromTimeInterval(interval: TimeInterval) -> String {
        let interval = Int(interval/1000)
        let seconds = interval % 60
        let minutes = (interval / 60) % 60
        let hours = (interval / 3600)
        return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
    }
    //MARK:- Button Action
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
//        let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "TestListView") as! TestListView
//      //  secondVc.PaperID = self.Get_PaperID
//        self.navigationController?.pushViewController(secondVc, animated: true)
    }
    
    @IBAction func btnSummaryAction(_ sender: UIButton) {
        let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "TestSummaryView") as! TestSummaryView
        secondVc.strID = PaperID
        
        //let option_list = Test_Report_List["level_questions"] as? [[String : Any]] ?? []
        //secondVc.dataLevel = option_list
        self.navigationController?.pushViewController(secondVc, animated: true)
    }
    
    //MARK:- Tableview methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:TestReportTableCell = tableView.dequeueReusableCell(withIdentifier: "TestReportTableCell") as! TestReportTableCell
        cell.layoutIfNeeded()
        cell.vwBG.setShadow()
        
        cell.layoutIfNeeded()
        cell.selectionStyle = .none
        
        
        if  test_details1["subjectaccuracy"] != nil {
            
            let idTo = Float((test_details1["subjectaccuracy"]! as! NSString).floatValue)
            let floatid = Float(idTo/100)
            cell.ProgressView.setProgress(floatid, animated: true)
            print("PROCESS:",cell.ProgressView.progress);
            var String1 = "Your Overall Subject Accuracy: "
            String1 += test_details1["subjectaccuracy"]! as! String
            String1 += "%"
            
            cell.AccuracyLabel.text = String1
        }
       
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var lastInitialDisplayableCell = false
        if 3 > 0 && !finishedLoadingInitialTableCells {
            if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        
        if !finishedLoadingInitialTableCells {
            if lastInitialDisplayableCell {
                finishedLoadingInitialTableCells = true
            }
            cell.loadAnimation()
        }
    }
    
    //MARK:- Collection view methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let option_list = Test_Report_List["level_questions"] as? [Any] else {
            
            return 0
        }
        return option_list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TestReportLevelcolletionCell", for: indexPath) as! TestReportLevelcolletionCell
        cell.layoutIfNeeded()

        cell.lblRadius.setRadius(radius: cell.lblRadius.frame.height/2)
        cell.lblRadius.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.lightGreen))
        
        cell.lblLevelTitle.text = ((Test_Report_List["level_questions"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "Level") as? String ?? ""
        cell.lblRadius.text = "\(((Test_Report_List["level_questions"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "Count") as? Int ?? 0)"

        cell.layoutIfNeeded()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.width/3 - 10, height: collectionView.frame.width/3 + 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        var lastInitialDisplayableCell = false
        if 3 > 0 && !finishedLoadingInitialCollectionCells {
            if let indexPathsForVisibleRows = collectionView.indexPathsForVisibleItems.last,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        
        if !finishedLoadingInitialCollectionCells {
            if lastInitialDisplayableCell {
                finishedLoadingInitialCollectionCells = true
            }
            cell.loadAnimation()
        }
    }
}

//
//  ViewController.swift
//  Student
//
//  Created by My Mac on 26/01/19.
//  Copyright © 2019 My Mac. All rights reserved.
// #FF5070 , #F8724F

import UIKit

class colletionViewCell: UICollectionViewCell {
  
    @IBOutlet weak var vwBG: UIView!
    @IBOutlet weak var imgOutlet: UIImageView!
    @IBOutlet weak var lblOutlet: UILabel!
}

class DashboardViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var imgGredient: UIImageView!
    
    var finishedLoadingInitialCollectionCells = false

   var imageArray = [UIImage(named: "02"),UIImage(named: "03"),UIImage(named: "04"),UIImage(named: "05")]
    @IBOutlet weak var myColletionView: UICollectionView!
  //  let DataArray = ["ASSIGNMENT","SEARCH","REPORTS","ZOOKI"]
     
    let DataArray = ["ASSIGNMENT","ASSESSMENT","SEARCH","REPORTS"]
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgGredient.setGredient()
        
       // myColletionView.backgroundColor = UIColor.darkerRed()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        self.navigationController?.isNavigationBarHidden = true
    }
    
     //MARK:- Button Action Methods
    
    @IBAction func menuBtnPressed(_ sender: UIButton) {
        let popOverVC = self.storyboard?.instantiateViewController(withIdentifier: "MenuPageViewController") as! MenuPageViewController
        
        self.addChild(popOverVC)
        //let sd  = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        
        popOverVC.view.frame = self.view.frame
       
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
        // self.view.bringSubviewToFront(popOverVC.view)
        //UIApplication.shared.keyWindow!.bringSubviewToFront(popOverVC.view)
    }
    
    //MARK:- Collection view methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myCell", for: indexPath) as! colletionViewCell
        cell.layoutIfNeeded()
        cell.lblOutlet.text = DataArray[indexPath.row]
        cell.imgOutlet.image = imageArray[indexPath.row]
        
        if indexPath.row == 0
        {
            cell.vwBG.roundCorners(corners: [.topLeft , .topRight , .bottomLeft], radius: 15)
        }
        else if indexPath.row == 1
        {
            cell.vwBG.roundCorners(corners: [.topLeft , .topRight , .bottomRight], radius: 15)
        }
        else if indexPath.row == 2
        {
            cell.vwBG.roundCorners(corners: [.topLeft , .bottomRight , .bottomLeft], radius: 15)
        }
        else
        {
            cell.vwBG.roundCorners(corners: [.topRight , .bottomRight , .bottomLeft], radius: 15)
        }
        
        cell.layoutIfNeeded()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if indexPath.row == 0
//        {
//            let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "TestListView") as! TestListView
//            self.navigationController?.pushViewController(secondVc, animated: true)
//        }
//        else if indexPath.row == 1
//        {
//            let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "searchTestPapersController") as! searchTestPapersController
//            self.navigationController?.pushViewController(secondVc, animated: true)
//        }
//
//        else if indexPath.row == 2
//        {
//            let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "ReportViewController") as! ReportViewController
//            self.navigationController?.pushViewController(secondVc, animated: true)
//        }
//
//        else if indexPath.row == 3
//        {
//            let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "StoryboardViewController") as! StoryboardViewController
//            self.navigationController?.pushViewController(secondVc, animated: true)
//        }
        
        
        if indexPath.row == 0
             {
                 let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "TestListView") as! TestListView
                 self.navigationController?.pushViewController(secondVc, animated: true)
             }
            else if indexPath.row == 1
                        {
                            let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "AllAssessmentViewController") as! AllAssessmentViewController
                            self.navigationController?.pushViewController(secondVc, animated: true)
                        }
             else if indexPath.row == 2
             {
                 let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "searchTestPapersController") as! searchTestPapersController
                 self.navigationController?.pushViewController(secondVc, animated: true)
             }
                 
             else if indexPath.row == 3
             {
                 let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "ReportViewController") as! ReportViewController
                 self.navigationController?.pushViewController(secondVc, animated: true)
             }
                 
            
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.width/2 - 10, height: collectionView.frame.width/2)
    }
   
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        var lastInitialDisplayableCell = false
        if DataArray.count > 0 && !finishedLoadingInitialCollectionCells {
            if let indexPathsForVisibleRows = collectionView.indexPathsForVisibleItems.last,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        
        if !finishedLoadingInitialCollectionCells {
            if lastInitialDisplayableCell {
                finishedLoadingInitialCollectionCells = true
            }
            cell.loadAnimation()
        }
    }
    @IBAction func notification_click(_ sender: UIButton) {
        let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "NoticationViewController") as! NoticationViewController
        self.navigationController?.pushViewController(secondVc, animated: true)
    }
}

extension UIColor {
    
    func add(overlay: UIColor) -> UIColor {
        var bgR: CGFloat = 0
        var bgG: CGFloat = 0
        var bgB: CGFloat = 0
        var bgA: CGFloat = 0
        
        var fgR: CGFloat = 0
        var fgG: CGFloat = 0
        var fgB: CGFloat = 0
        var fgA: CGFloat = 0
        
        self.getRed(&bgR, green: &bgG, blue: &bgB, alpha: &bgA)
        overlay.getRed(&fgR, green: &fgG, blue: &fgB, alpha: &fgA)
        
        let r = fgA * fgR + (1 - fgA) * bgR
        let g = fgA * fgG + (1 - fgA) * bgG
        let b = fgA * fgB + (1 - fgA) * bgB
        
        return UIColor(red: r, green: g, blue: b, alpha: 1.0)
    }
}

//
//  searchTestPapersController.swift
//  Student
//
//  Created by My Mac on 02/02/19.
//  Copyright © 2019 My Mac. All rights reserved.
// 0.5 progesive

import UIKit
import Alamofire
import SVProgressHUD
import SDWebImage

class searchTestCell: UITableViewCell {
    @IBOutlet weak var vwBG: UIView!
    @IBOutlet weak var vwRounder: UIView!
    
    @IBOutlet weak var lblRound1: UILabel!
    @IBOutlet weak var lblRound2: UILabel!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblAccuracy: UILabel!
    @IBOutlet weak var progresiveOutlet: UIProgressView!
}



class searchAssignmentCell: UITableViewCell {
    
    @IBOutlet weak var vwBG: UIView!
    
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
}

class searchTestPapersController: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var imgBG: UIImageView!
    
    @IBOutlet weak var imgBlur: UIImageView!
    @IBOutlet weak var segSearch: UISegmentedControl!
    @IBOutlet weak var vwPopupSearch: UIView!
    
    @IBOutlet weak var tblSearch: UITableView!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var imgBGSave: UIImageView!
    @IBOutlet weak var btnSave: UIButton!
    
    var levelList = [[String : Any]]()
    var subject_listArray = [[String: Any]]()
    var search_list_Array = [[String: Any]]()

    var finishedLoadingInitialTableCells = false
    var Click_List_Array = [String]()
    var Click_Subject_Array = [String]()
   
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //00101
         Subject_List_Api()
         Level_List_Api()
         Search_List_Api()
        
        imgBG.setGredient()
        tblView.tableFooterView = UIView()
        tblView.setRadius(radius: 10)
        vwPopupSearch.setRadius(radius: 10)
        
        btnReset.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.navigationColor))
        btnReset.setRadius(radius: 10)
        btnSave.setRadius(radius: 10)
        imgBGSave.setGredient()
        imgBGSave.setRadius(radius: 10)
        segSearch.setRadius(radius: segSearch.frame.height/2)

        segSearch.removeBorders()
        
        let font = UIFont.boldSystemFont(ofSize: 17)
        segSearch.setTitleTextAttributes([NSAttributedString.Key.font: font],
                                         for: .normal)
        tblView.rowHeight = UITableView.automaticDimension
        tblView.estimatedRowHeight = 600
    }
    
     //MARK:- WEB SERVICES
    
    func Level_List_Api()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            SVProgressHUD.show(withStatus: nil)
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["student"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.LEVEL_LIST)!,
                              method: .get,
                              parameters: nil,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : Any] = [:]
                
                        guard let data = response.result.value as? [String:Any],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        if resData["status"] as? Bool ?? false
                        {
                            self.levelList = resData["data"] as! [[String : Any]]
                           
                        }
                        
//                        DispatchQueue.main.async {
//                            self.tblSearch.reloadData()
//                        }
                        
                        self.view.layoutIfNeeded()
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    
    
    func Subject_List_Api()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            SVProgressHUD.show(withStatus: nil)
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["student"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.SUBJECT_LIST)!,
                              method: .post,
                              parameters: nil,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : Any] = [:]
                       
                        
                        guard let data = response.result.value as? [String:Any],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        if resData["status"] as? Bool ?? false
                        {
                            self.subject_listArray = resData["data"] as! [[String : Any]]
                            
                        }
                        
                        DispatchQueue.main.async {
                            self.tblSearch.reloadData()
                        }
                        
                        self.view.layoutIfNeeded()
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    
    
    func Search_List_Api()
    {
        if !isInternetAvailable() {
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            SVProgressHUD.show(withStatus: nil)
            let parameters = ["LevelID" : Click_List_Array.joined(separator: ",") ,
                              "SubjectID" : Click_Subject_Array.joined(separator: ",")] as [String : Any]
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["student"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.STUDENT_SEARCH_PAPER)!,
                              method: .post,
                              parameters: parameters,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : Any] = [:]
                        
                        
                        guard let data = response.result.value as? [String:Any],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        self.search_list_Array.removeAll()
                        if resData["status"] as? Bool ?? false
                        {
                            self.search_list_Array = resData["data"] as! [[String : Any]]
                        }
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                        self.tblView.reloadData()
//                        DispatchQueue.main.async {
//
//                        }
                        
                        self.view.layoutIfNeeded()
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    
    
    func setDateWithFormat(strDate : String) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d-MMM-yyyy"
        //"MMM dd ,yyyy HH:mm"
        let date: Date? = dateFormatterGet.date(from: strDate)!
        return dateFormatter.string(from: date!)
    }

    /*
     
     cell.btnSelection.setTitle(selectedData.contains(cell.lblName.text ?? "") ? "✓" : "", for: .normal)
     //another code
     
     if arrSelected.contains((dictData["option_values"] as AnyObject).objectAt(indexPath.row) as? String ?? "")
     {
     arrSelected = arrSelected.filter{$0 != (dictData["option_values"] as AnyObject).objectAt(indexPath.row) as? String ?? ""}
     }
     else
     {
     arrSelected.append((dictData["option_values"] as AnyObject).objectAt(indexPath.row) as? String ?? "")
     }
     
     tblVW.reloadData()
    */
    //MARK:- Tableview methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblSearch
        {
             if segSearch.selectedSegmentIndex == 0 {
                return subject_listArray.count
            }
                return levelList.count
        }
            
        else {
             return search_list_Array.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == tblSearch
        {
            let cell:searchAssignmentCell = tableView.dequeueReusableCell(withIdentifier: "searchAssignmentCell") as! searchAssignmentCell
            cell.layoutIfNeeded()
            
            cell.btnSelect.setRadius(radius: cell.btnSelect.frame.height/2)
            cell.btnSelect.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.navigationColor))
            if indexPath.row == 0
            {
                cell.vwBG.roundCorners(corners: [.topLeft , .topRight], radius: 10)
            }
            else if indexPath.row == 3
            {
                cell.vwBG.roundCorners(corners: [.bottomLeft , .bottomRight], radius: 10)
            }
            else
            {
                cell.vwBG.roundCorners(corners: [.bottomLeft , .bottomRight], radius: 0)
            }
            
             if segSearch.selectedSegmentIndex == 0 {
                cell.lblTitle.text = subject_listArray[indexPath.row]["SubjectName"] as? String ?? ""
                
                cell.btnSelect.setTitle(Click_Subject_Array.contains(subject_listArray[indexPath.row]["SubjectID"] as? String ?? "") ? "✓" : "", for: .normal)
            }
                
             else {
                cell.lblTitle.text = levelList[indexPath.row]["LevelName"] as? String ?? ""
                 cell.btnSelect.tag = indexPath.row
              
                cell.btnSelect.setTitle(Click_List_Array.contains(levelList[indexPath.row]["LevelID"] as? String ?? "") ? "✓" : "", for: .normal)
            }
            
            cell.layoutIfNeeded()
            cell.selectionStyle = .none
            return cell
        }
            
        else
        {
            let cell:searchTestCell = tableView.dequeueReusableCell(withIdentifier: "myCell") as! searchTestCell
            cell.layoutIfNeeded()

            cell.lblTitle.text = search_list_Array[indexPath.row]["SubjectName"] as? String ?? ""
            
            cell.lblRound1.text = search_list_Array[indexPath.row]["TotalRight"] as? String ?? ""
        
            cell.lblRound2.text = search_list_Array[indexPath.row]["TotalQuestion"] as? String ?? ""
            
            let df = search_list_Array[indexPath.row]["SubmitDate"] as? String ?? ""
            let getDate = setDateWithFormat(strDate: df)
            cell.lblDate.text = getDate
            
            cell.vwRounder.setRadius(radius: cell.vwRounder.frame.height/2)
            
            if indexPath.row == 0
            {
                cell.vwBG.roundCorners(corners: [.topLeft , .topRight], radius: 10)
            }
            else if indexPath.row == 3
            {
                cell.vwBG.roundCorners(corners: [.bottomLeft , .bottomRight], radius: 10)
            }
            else
            {
                cell.vwBG.roundCorners(corners: [.bottomLeft , .bottomRight], radius: 0)
            }
            
            
            cell.layoutIfNeeded()
            cell.selectionStyle = .none
            return cell
        }
    }
    
    // SubjectID , SubjectName
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          if tableView == tblSearch
          {
            
            if segSearch.selectedSegmentIndex == 0 {
           
                if Click_Subject_Array.contains(subject_listArray[indexPath.row]["SubjectID"] as? String ?? "")
                {
                    Click_Subject_Array = Click_Subject_Array.filter{$0 != (subject_listArray[indexPath.row]["SubjectID"] as? String ?? "")}
                }
                else
                {
                    Click_Subject_Array.append(subject_listArray[indexPath.row]["SubjectID"] as? String ?? "")
                }
           
            }
                
             else {
              
                 if Click_List_Array.contains(levelList[indexPath.row]["LevelID"] as? String ?? "")
                 {
                     Click_List_Array = Click_List_Array.filter{$0 != (levelList[indexPath.row]["LevelID"] as? String ?? "")}
                 }
                   else
                   {
                      Click_List_Array.append(levelList[indexPath.row]["LevelID"] as? String ?? "")
                   }
                }
            
               tblSearch.reloadData()
            }
         
          else {
                  let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "TestReportView") as! TestReportView
                  secondVc.PaperID = search_list_Array[indexPath.row]["StudentMCQTestID"] as? String ?? ""
                 self.navigationController?.pushViewController(secondVc, animated: true)
             }
        }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var lastInitialDisplayableCell = false
        if 4 > 0 && !finishedLoadingInitialTableCells {
            if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        
        if !finishedLoadingInitialTableCells {
            if lastInitialDisplayableCell {
                finishedLoadingInitialTableCells = true
            }
            cell.loadAnimation()
        }
    }
    
    //MARK:- Button Action
    
    @IBAction func segmentClicked(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            self.tblSearch.reloadData()
        }
        
        if sender.selectedSegmentIndex == 1 {
           // Click_List_Array.removeAll()
            self.tblSearch.reloadData()
        }
    }
    
    @IBAction func bttActionClicked(_ sender: UIButton) {
        let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
      //  secondVc.PaperID = self.Get_PaperID
        self.navigationController?.pushViewController(secondVc, animated: true)
    }
    
    @IBAction func btnSearchAction(_ sender: UIButton) {
        showViewWithAnimation(vw: vwPopupSearch, img: imgBlur)
    }
    
    @IBAction func btnCloseAction(_ sender: UIButton) {
        hideViewWithAnimation(vw: vwPopupSearch, img: imgBlur)
    }
    
    @IBAction func btnResetAction(_ sender: UIButton) {
        
        Click_List_Array.removeAll()
        Click_Subject_Array.removeAll()
        self.tblSearch.reloadData()
    }
    
    @IBAction func btnSaveAction(_ sender: UIButton) {
          hideViewWithAnimation(vw: vwPopupSearch, img: imgBlur)
           Search_List_Api()
    }
    
}


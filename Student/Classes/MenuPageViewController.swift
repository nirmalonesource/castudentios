//
//  MenuPageViewController.swift
//  Student
//
//  Created by My Mac on 02/02/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit


class MenuCell: UITableViewCell {
    
    @IBOutlet weak var LblList: UILabel!
}

class MenuPageViewController: UIViewController ,UITableViewDelegate, UITableViewDataSource {
    let ListArray = ["DASHBOARD","MY PROFILE","QUERY? CALL US","RATE US","LEGAL","LOG OUT"]
    
    @IBOutlet weak var btnCrossOutlet: UIButton!
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var popView: UIView!
    @IBOutlet weak var tblView: UITableView!
     @IBOutlet weak var imgBlur: UIImageView!
      @IBOutlet weak var heightConstrants: NSLayoutConstraint!
    
     //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgBlur.setGredient()
        popView.roundCorners(corners: [.bottomRight], radius: popView.frame.height/2)
        self.tblView.tableFooterView = UIView()
        let gestureRecognizerOne = UITapGestureRecognizer(target: self, action: #selector(tap))
        imgBlur.addGestureRecognizer(gestureRecognizerOne)
        self.view.layoutIfNeeded()
    }
    
    @objc func tap() {
        hideViewWithAnimation(vw: mainView, img: imgBlur)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.layoutIfNeeded()
        heightConstrants.constant = tblView.contentSize.height
        popView.roundCorners(corners: [.bottomRight], radius: popView.frame.height/2)
        self.view.layoutIfNeeded()
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath) as! MenuCell
        cell.LblList.text = ListArray[indexPath.row]
         cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
            
        else if indexPath.row == 1 {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileView") as! MyProfileView
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        
        else if indexPath.row == 2 {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "QueryView") as! QueryView
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        
        else if indexPath.row == 3 {
            let urlStr = Appstore.APPSTOREREVIEW_LINK // (Option 2) Open App Review Tab
        
            if let url = URL(string: urlStr), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        // let urlStr = "itms-apps://itunes.apple.com/app/id\(appID)" // (Option 1) Open App Page
       
        
        else if indexPath.row == 4 {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "LegalViewController") as! LegalViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        else if indexPath.row == 5 {
            
            let alert = EMAlertController(title: ConstantVariables.Constants.Project_Name, message: "Are you sure you want to Logout?")
            
            let action1 = EMAlertAction(title: "Logout", style: .cancel)
            {
                let domain = Bundle.main.bundleIdentifier!
                UserDefaults.standard.removePersistentDomain(forName: domain)
                UserDefaults.standard.synchronize()
                print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
                
                let loginView: LoginView = self.storyboard!.instantiateViewController(withIdentifier: "LoginView") as! LoginView
                let navController = UINavigationController.init(rootViewController: loginView)
                UIApplication.shared.keyWindow?.rootViewController = navController
            }
            
            let action2 = EMAlertAction(title: "Cancel", style: .normal) {
                // Perform Action
            }
            
            alert.addAction(action: action1)
            alert.addAction(action: action2)
            
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func crossButtonPressed(_ sender: UIButton) {
         mainView.isHidden = !mainView.isHidden
    }
    
    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

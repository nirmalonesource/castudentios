//
//  LoginView.swift
//  TeacherApp
//
//  Created by My Mac on 23/01/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class LoginView: UIViewController , UITextFieldDelegate {

    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var imgSubmitBG: UIImageView!
    @IBOutlet weak var txtPhone: UITextField!
    
    @IBOutlet weak var segmentVW: UISegmentedControl!
    
    //MARK:- View Life Cyycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        imgBG.setGredient()
        segmentVW.setRadius(radius: segmentVW.frame.height/2)
        btnSubmit.setRadius(radius: btnSubmit.frame.height/2)
        imgLogo.setRadius(radius: imgLogo.frame.height/2)
        imgSubmitBG.setRadius(radius: imgSubmitBG.frame.height/2)
        btnSubmit.setGredientText()
        btnSubmit.dampAnimation()
        
        segmentVW.removeBorders()

        let font = UIFont.boldSystemFont(ofSize: 17)
        segmentVW.setTitleTextAttributes([NSAttributedString.Key.font: font],
                                                for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK:- WEB SERVICES
    
    func getlogin()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            SVProgressHUD.show(withStatus: nil)
            let DeviceID = UIDevice.current.identifierForVendor?.uuidString
            let parameters = ["StudentCode" : txtPhone.text!,
                              "DeviceID": DeviceID!] as [String : Any]
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.LOGIN)!,
                              method: .post,
                              parameters: parameters,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        if resData["status"] as? Bool ?? false
                        {
                            var dict = resData["data"] as? [String : Any] ?? [:]
                            let otp = dict["otp"] as? Int ?? 0
                            let mobileno = dict["mobileno"] as? String ?? "0000000000"
                            print(" otp is: \(otp)")
                            if otp != 0 {
                                let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "OTPVerificationView") as! OTPVerificationView
                                secondVc.otpGet = otp
                                secondVc.mobilenoGet = mobileno

                                secondVc.StudentCodeGet = self.txtPhone.text!
                                self.navigationController?.pushViewController(secondVc, animated: true)
                            }
                                
                            else {
                                print("Please Enter otp number")
                            }
                        }
                        
                        showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }

    func validation()
    {
        self.view.endEditing(true)
        
        if txtPhone.text == ""
        {
            showAlert(uiview: self, msg: ConstantVariables.LocalizeString.PLEASE_ENTER + " " + ConstantVariables.LocalizeString.STUDENT_CODE, isTwoButton: false)
        }
        else {
            getlogin()
        }
    }
    
    @IBAction func btnSubmitAction(_ sender: UIButton) {
        validation()
    }
    
    //MARK:- Textfield Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

extension UISegmentedControl {
    func removeBorders() {
        setBackgroundImage(imageWithColor(color: backgroundColor!), for: .normal, barMetrics: .default)
        setBackgroundImage(imageWithColor(color: tintColor!), for: .selected, barMetrics: .default)
        setDividerImage(imageWithColor(color: UIColor.clear), forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
    }
    
    // create a 1x1 image with this color
    private func imageWithColor(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width:  1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor);
        context!.fill(rect);
        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return image!
    }
}
